use criterion::{criterion_group, criterion_main, Criterion};
extern crate thesis_experimentation;

use specs::prelude::*;
use thesis_experimentation::exp1::oop_obj::*;
use thesis_experimentation::exp1::oop::*;
use thesis_experimentation::exp1::dod::*;
use std::time::Duration;

#[inline]
fn dod_dispatch(d: &mut Dispatcher, mut w: &mut World) {
    d.dispatch_par(&mut w);
}

#[inline]
fn oop_dispatch<T: Exp1>(world: &mut OOPWorld<T>) { world.execute(); }

pub fn oop_criterion_benchmark(c: &mut Criterion) {
    let mut group = c.benchmark_group("oop_exp1");
    group.warm_up_time(Duration::from_secs(5));
    group.sample_size(100);
    group.nresamples(100);
    rayon::ThreadPoolBuilder::new().num_threads(1).build_global().unwrap();

    let o128 = obj_setup::<Obj128>();
    let o256 = obj_setup::<Obj256>();
    let o512 = obj_setup::<Obj512>();
    let o1024 = obj_setup::<Obj1024>();
    let o2048 = obj_setup::<Obj2048>();
    let o4196 = obj_setup::<Obj4096>();
    let o8192 = obj_setup::<Obj8192>();
    let o16384 = obj_setup::<Obj16384>();

    let mut world128 = OOPWorld::new(o128, 1);
    let mut world256 = OOPWorld::new(o256, 1);
    let mut world512 = OOPWorld::new(o512, 1);
    let mut world1024 = OOPWorld::new(o1024, 1);
    let mut world2048 = OOPWorld::new(o2048, 1);
    let mut world4196 = OOPWorld::new(o4196, 1);
    let mut world8192 = OOPWorld::new(o8192, 1);
    let mut world16384 = OOPWorld::new(o16384, 1);

    group.bench_function("oop_exp1_size_128", |b|
        b.iter(||oop_dispatch(&mut world128)));
    group.bench_function("oop_exp1_size_256", |b|
        b.iter(||oop_dispatch(&mut world256)));
    group.bench_function("oop_exp1_size_512", |b|
        b.iter(||oop_dispatch(&mut world512)));
    group.bench_function("oop_exp1_size_1024", |b|
        b.iter(||oop_dispatch(&mut world1024)));
    group.bench_function("oop_exp1_size_2048", |b|
        b.iter(||oop_dispatch(&mut world2048)));
    group.bench_function("oop_exp1_size_4196", |b|
        b.iter(||oop_dispatch(&mut world4196)));
    group.bench_function("oop_exp1_size_8192", |b|
        b.iter(||oop_dispatch(&mut world8192)));
    group.bench_function("oop_exp1_size_16384", |b|
        b.iter(||oop_dispatch(&mut world16384)));
}

pub fn dod_criterion_benchmark(c: &mut Criterion) {
    let mut group = c.benchmark_group("dod_exp1");
    group.warm_up_time(Duration::from_secs(5));
    group.sample_size(100);
    group.nresamples(100);

    let entity_size: Vec<i32> = vec![128,256,512,1024,2048,4096,8192,16384];

    entity_size.iter().for_each(|size| {
        let mut world = World::new();
        setup_component(&mut world).unwrap();
        setup_entity(*size, &mut world).unwrap();
        let mut dispatcher = setup_dispatcher(*size);

        let mut bench_name = String::from("dod_exp1_size_");
        let i = size.to_string();
        bench_name.push_str(&i);

        group.bench_function(bench_name, |b|
            b.iter( || dod_dispatch(&mut dispatcher, &mut world)));
    });
}
criterion_group!(oop, oop_criterion_benchmark);
criterion_group!(dod, dod_criterion_benchmark);
criterion_main!(oop,dod);