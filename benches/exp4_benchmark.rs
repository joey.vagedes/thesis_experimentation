use criterion::{criterion_group, criterion_main, Criterion};
use std::time::Duration;
use specs::prelude::*;
use thesis_experimentation::exp4::dod::*;
use thesis_experimentation::exp4::oop::*;
use thesis_experimentation::exp4::oop_obj::*;

#[inline]
fn dod_dispatch(d: &mut Dispatcher, mut w: &mut World) {
    d.dispatch_par(&mut w);
}

#[inline]
fn oop_dispatch<T: Exp4>(world: &mut OOPWorld<T>) { world.execute(); }

pub fn dod_criterion_benchmark(c: &mut Criterion) {
    let mut group = c.benchmark_group("dod_exp4");
    group.warm_up_time(Duration::from_secs(5));
    group.sample_size(100);
    group.nresamples(100);

    let thread_count = vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];

    thread_count.iter().for_each(|count|{

        let mut world = World::new();
        setup_component(&mut world).unwrap();
        setup_entity(&mut world).unwrap();
        let mut dispatcher = setup_dispatcher(*count);

        dispatcher.setup(&mut world);

        let mut bench_name = String::from("dod_exp4_thread_count_");
        let i = count.to_string();
        bench_name.push_str(&i);

        group.bench_function(bench_name.as_str(), |b| b.iter(|| dod_dispatch(&mut dispatcher, &mut world)));

    });
}

fn oop_criterion_benchmark(c: &mut Criterion) {
    let mut group = c.benchmark_group("oop_exp4");
    group.warm_up_time(Duration::from_secs(5));
    group.sample_size(100);
    group.nresamples(100);

    let thread_count = vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];

    thread_count.iter().for_each(|count| {
        let vec = obj_setup::<Obj2048>(1000);
        let mut world = OOPWorld::new(vec, *count);

        let mut bench_name = String::from("oop_exp4_thread_count_");
        let i = count.to_string();
        bench_name.push_str(&i);

        group.bench_function(bench_name.as_str(), |b| b.iter(||oop_dispatch(&mut world)));
    });
}

criterion_group!(dod_exp4, dod_criterion_benchmark);
criterion_group!(oop_exp4, oop_criterion_benchmark);
criterion_main!(dod_exp4, oop_exp4);