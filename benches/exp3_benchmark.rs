use criterion::{criterion_group, criterion_main, Criterion};
use std::time::Duration;
use thesis_experimentation::exp3::oop::*;
use thesis_experimentation::exp3::dod::*;
use thesis_experimentation::exp3::oop_obj::*;
use specs::prelude::*;

#[inline]
fn oop_dispatch<T: Exp3>(world: &mut OOPWorld<T>) { world.execute(); }

#[inline]
fn dod_dispatch(d: &mut Dispatcher, mut w: &mut World) {
    d.dispatch_par(&mut w);
}

fn oop_criterion_benchmark(c: &mut Criterion) {
    let mut group = c.benchmark_group("oop_exp3");
    group.warm_up_time(Duration::from_secs(5));
    group.sample_size(100);
    group.nresamples(100);
    rayon::ThreadPoolBuilder::new().num_threads(1).build_global().unwrap();

    let entity_count = vec![10, 50, 100, 500, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000];

    entity_count.iter().for_each(|count|{
        let vec = obj_setup::<Obj128>(*count);
        let mut world = OOPWorld::new(vec, 1);

        let mut bench_name = String::from("oop_exp3_count_");
        let i = count.to_string();
        bench_name.push_str(&i);
        bench_name.push_str("_size_128");

        group.bench_function(bench_name.as_str(), |b| b.iter(||oop_dispatch(&mut world)));
    });

    entity_count.iter().for_each(|count|{
        let vec = obj_setup::<Obj256>(*count);
        let mut world = OOPWorld::new(vec, 1);

        let mut bench_name = String::from("oop_exp3_count_");
        let i = count.to_string();
        bench_name.push_str(&i);
        bench_name.push_str("_size_256");

        group.bench_function(bench_name.as_str(), |b| b.iter(||oop_dispatch(&mut world)));
    });

    entity_count.iter().for_each(|count|{
        let vec = obj_setup::<Obj512>(*count);
        let mut world = OOPWorld::new(vec, 1);

        let mut bench_name = String::from("oop_exp3_count_");
        let i = count.to_string();
        bench_name.push_str(&i);
        bench_name.push_str("_size_512");

        group.bench_function(bench_name.as_str(), |b| b.iter(||oop_dispatch(&mut world)));
    });

    entity_count.iter().for_each(|count|{
        let vec = obj_setup::<Obj1024>(*count);
        let mut world = OOPWorld::new(vec, 1);

        let mut bench_name = String::from("oop_exp3_count_");
        let i = count.to_string();
        bench_name.push_str(&i);
        bench_name.push_str("_size_1024");

        group.bench_function(bench_name.as_str(), |b| b.iter(||oop_dispatch(&mut world)));
    });

    entity_count.iter().for_each(|count|{
        let vec = obj_setup::<Obj2048>(*count);
        let mut world = OOPWorld::new(vec, 1);

        let mut bench_name = String::from("oop_exp3_count_");
        let i = count.to_string();
        bench_name.push_str(&i);
        bench_name.push_str("_size_2048");

        group.bench_function(bench_name.as_str(), |b| b.iter(||oop_dispatch(&mut world)));
    });

    entity_count.iter().for_each(|count|{
        let vec = obj_setup::<Obj4096>(*count);
        let mut world = OOPWorld::new(vec, 1);

        let mut bench_name = String::from("oop_exp3_count_");
        let i = count.to_string();
        bench_name.push_str(&i);
        bench_name.push_str("_size_4096");

        group.bench_function(bench_name.as_str(), |b| b.iter(||oop_dispatch(&mut world)));
    });

    entity_count.iter().for_each(|count|{
        let vec = obj_setup::<Obj8192>(*count);
        let mut world = OOPWorld::new(vec, 1);

        let mut bench_name = String::from("oop_exp3_count_");
        let i = count.to_string();
        bench_name.push_str(&i);
        bench_name.push_str("_size_8192");

        group.bench_function(bench_name.as_str(), |b| b.iter(||oop_dispatch(&mut world)));
    });

    entity_count.iter().for_each(|count|{
        let vec = obj_setup::<Obj16384>(*count);
        let mut world = OOPWorld::new(vec, 1);

        let mut bench_name = String::from("oop_exp3_count_");
        let i = count.to_string();
        bench_name.push_str(&i);
        bench_name.push_str("_size_16384");

        group.bench_function(bench_name.as_str(), |b| b.iter(||oop_dispatch(&mut world)));
    });
}

pub fn dod_criterion_benchmark(c: &mut Criterion) {
    let mut group = c.benchmark_group("dod_exp3");
    group.warm_up_time(Duration::from_secs(5));
    group.sample_size(100);
    group.nresamples(100);

    let entity_size: Vec<i32> = vec![128,256,512,1024,2048,4096,8192,16384];
    let entity_count: Vec<i32> = vec![10, 50, 100, 500, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000];

    entity_size.iter().for_each(|size| {
        entity_count.iter().for_each(|count| {
            let mut world = World::new();
            setup_component(&mut world).unwrap();
            setup_entity(*size, *count, &mut world).unwrap();
            let mut dispatcher = setup_dispatcher(*size);
            let mut bench_name = String::from("dod_exp3_count_");
            let i = count.to_string();
            bench_name.push_str(&i);
            bench_name.push_str("_size_");
            let i = size.to_string();
            bench_name.push_str(&i);

            group.bench_function(bench_name, |b| b.iter( || dod_dispatch(&mut dispatcher, &mut world)));
        })
    });
}

criterion_group!(oop_exp3, oop_criterion_benchmark);
criterion_group!(dod_exp3, dod_criterion_benchmark);
criterion_main!(oop_exp3, dod_exp3);