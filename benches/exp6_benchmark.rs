use criterion::{criterion_group, criterion_main, Criterion};
use std::time::Duration;
use thesis_experimentation::exp6::oop::*;
use thesis_experimentation::exp6::oop_obj::*;
use thesis_experimentation::exp6::dod::*;
use specs::prelude::*;

#[inline]
fn oop_dispatch<T: Exp6>(world: &mut OOPWorld<T>) { world.execute(); }

#[inline]
fn dod_dispatch(d: &mut Dispatcher, mut w: &mut World) {
    d.dispatch_par(&mut w);
}

fn oop_criterion_benchmark(c: &mut Criterion) {
    let mut group = c.benchmark_group("oop_exp6");
    group.warm_up_time(Duration::from_secs(5));
    group.sample_size(100);
    group.nresamples(100);
    rayon::ThreadPoolBuilder::new().num_threads(1).build_global().unwrap();

    let o2048 = obj_setup::<Exp2048>();
    let o1912 = obj_setup::<Exp1912>();
    let o1792 = obj_setup::<Exp1792>();
    let o1664 = obj_setup::<Exp1664>();
    let o1536 = obj_setup::<Exp1536>();
    let o1408 = obj_setup::<Exp1408>();
    let o1280 = obj_setup::<Exp1280>();
    let o1152 = obj_setup::<Exp1152>();
    let o1024 = obj_setup::<Exp1024>();
    let o896 = obj_setup::<Exp896>();
    let o768 = obj_setup::<Exp768>();
    let o640 = obj_setup::<Exp640>();
    let o512 = obj_setup::<Exp512>();
    let o384 = obj_setup::<Exp384>();
    let o256 = obj_setup::<Exp256>();

    let mut world2048 = OOPWorld::new(o2048);
    let mut world1912 = OOPWorld::new(o1912);
    let mut world1792 = OOPWorld::new(o1792);
    let mut world1664 = OOPWorld::new(o1664);
    let mut world1536 = OOPWorld::new(o1536);
    let mut world1408 = OOPWorld::new(o1408);
    let mut world1280 = OOPWorld::new(o1280);
    let mut world1152 = OOPWorld::new(o1152);
    let mut world1024 = OOPWorld::new(o1024);
    let mut world896 = OOPWorld::new(o896);
    let mut world768 = OOPWorld::new(o768);
    let mut world640 = OOPWorld::new(o640);
    let mut world512 = OOPWorld::new(o512);
    let mut world384 = OOPWorld::new(o384);
    let mut world256 = OOPWorld::new(o256);

    group.bench_function("oop_exp6_2048", |b| b.iter(||oop_dispatch(&mut world2048)));
    group.bench_function("oop_exp6_1912", |b| b.iter(||oop_dispatch(&mut world1912)));
    group.bench_function("oop_exp6_1792", |b| b.iter(||oop_dispatch(&mut world1792)));
    group.bench_function("oop_exp6_1664", |b| b.iter(||oop_dispatch(&mut world1664)));
    group.bench_function("oop_exp6_1536", |b| b.iter(||oop_dispatch(&mut world1536)));
    group.bench_function("oop_exp6_1408", |b| b.iter(||oop_dispatch(&mut world1408)));
    group.bench_function("oop_exp6_1280", |b| b.iter(||oop_dispatch(&mut world1280)));
    group.bench_function("oop_exp6_1152", |b| b.iter(||oop_dispatch(&mut world1152)));
    group.bench_function("oop_exp6_1024", |b| b.iter(||oop_dispatch(&mut world1024)));
    group.bench_function("oop_exp6_896", |b| b.iter(||oop_dispatch(&mut world896)));
    group.bench_function("oop_exp6_768", |b| b.iter(||oop_dispatch(&mut world768)));
    group.bench_function("oop_exp6_640", |b| b.iter(||oop_dispatch(&mut world640)));
    group.bench_function("oop_exp6_512", |b| b.iter(||oop_dispatch(&mut world512)));
    group.bench_function("oop_exp6_384", |b| b.iter(||oop_dispatch(&mut world384)));
    group.bench_function("oop_exp6_256", |b| b.iter(||oop_dispatch(&mut world256)));
}

pub fn dod_criterion_benchmark(c: &mut Criterion) {
    let mut group = c.benchmark_group("dod_exp6");
    group.warm_up_time(Duration::from_secs(5));
    group.sample_size(100);
    group.nresamples(100);

    let entity_state_count = vec![2048, 1912, 1792, 1664, 1536, 1408, 1280, 1152, 1024, 896, 768, 640, 512, 384, 256];

    entity_state_count.iter().for_each(|count| {
        let mut world = World::new();
        setup_component(&mut world).unwrap();
        setup_entity( &mut world).unwrap();
        let mut dispatcher = setup_dispatcher(*count);
        let mut bench_name = String::from("dod_exp6_");
        let i = count.to_string();
        bench_name.push_str(&i);
        group.bench_function(bench_name, |b| b.iter( || dod_dispatch(&mut dispatcher, &mut world)));
    });
}

criterion_group!(oop_exp6, oop_criterion_benchmark);
criterion_group!(dod_exp6, dod_criterion_benchmark);
criterion_main!(oop_exp6, dod_exp6);