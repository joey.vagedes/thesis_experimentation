use criterion::{criterion_group, criterion_main, Criterion};
use std::time::Duration;
use specs::prelude::*;
use thesis_experimentation::exp2::dod::*;
use thesis_experimentation::exp2::oop::*;
use thesis_experimentation::exp2::oop_obj::*;

#[inline]
fn dod_dispatch(d: &mut Dispatcher, mut w: &mut World) {
    d.dispatch_par(&mut w);
}

#[inline]
fn oop_dispatch<T: Exp2>(world: &mut OOPWorld<T>) { world.execute(); }

pub fn dod_criterion_benchmark(c: &mut Criterion) {
    let mut group = c.benchmark_group("dod_exp2");
    group.warm_up_time(Duration::from_secs(5));
    group.sample_size(100);
    group.nresamples(100);

    let entity_count = vec![10, 50, 100, 500, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000];

    entity_count.iter().for_each(|count|{
        let mut world = World::new();
        setup_component(&mut world).unwrap();
        setup_entity(*count, &mut world).unwrap();
        let mut dispatcher = setup_dispatcher();

        dispatcher.setup(&mut world);

        let mut bench_name = String::from("dod_exp2_entity_count_");
        let i = count.to_string();
        bench_name.push_str(&i);

        group.bench_function(bench_name.as_str(), |b| b.iter(|| dod_dispatch(&mut dispatcher, &mut world)));
    });
}

fn oop_criterion_benchmark(c: &mut Criterion) {
    let mut group = c.benchmark_group("oop_exp2");
    group.warm_up_time(Duration::from_secs(5));
    group.sample_size(100);
    group.nresamples(100);

    let entity_count = vec![10, 50, 100, 500, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000];

    entity_count.iter().for_each(|count|{
        let vec = obj_setup::<Obj1024>(*count);
        let mut world = OOPWorld::new(vec, 1);

        let mut bench_name = String::from("oop_exp2_entity_count_");
        let i = count.to_string();
        bench_name.push_str(&i);

        group.bench_function(bench_name.as_str(), |b| b.iter(||oop_dispatch(&mut world)));
    });
}

criterion_group!(dod_exp2, dod_criterion_benchmark);
criterion_group!(oop_exp2, oop_criterion_benchmark);
criterion_main!(dod_exp2, oop_exp2);