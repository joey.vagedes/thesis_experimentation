use specs::prelude::*;
use std::io;
use super::dod_component::*;
use super::dod_system::*;
use std::sync::Arc;

//Add components to the world
pub fn setup_component(world: &mut World)-> io::Result<()> {
    world.register::<Comp_i64_0>();
    world.register::<Comp_i64_1>();
    world.register::<Comp_i128_0>();
    world.register::<Comp_i128_1>();
    world.register::<Comp_i128_2>();
    world.register::<Comp_i128_3>();
    world.register::<Comp_i128_4>();
    world.register::<Comp_i128_5>();
    world.register::<Comp_i128_6>();
    world.register::<Comp_i128_7>();
    world.register::<Comp_i128_8>();
    world.register::<Comp_i128_9>();
    world.register::<Comp_i128_10>();
    world.register::<Comp_i128_11>();
    world.register::<Comp_i128_12>();
    world.register::<Comp_i128_13>();
    world.register::<Comp_i128_14>();
    world.register::<Comp_i128_15>();
    world.register::<Comp_i128_16>();
    world.register::<Comp_i128_17>();
    world.register::<Comp_i128_18>();
    world.register::<Comp_i128_19>();
    world.register::<Comp_i128_20>();
    world.register::<Comp_i128_21>();
    world.register::<Comp_i128_22>();
    world.register::<Comp_i128_23>();
    world.register::<Comp_i128_24>();
    world.register::<Comp_i128_25>();
    world.register::<Comp_i128_26>();
    world.register::<Comp_i128_27>();
    world.register::<Comp_i128_28>();
    world.register::<Comp_i128_29>();
    world.register::<Comp_i128_30>();
    world.register::<Comp_i128_31>();
    world.register::<Comp_i128_32>();
    world.register::<Comp_i128_33>();
    world.register::<Comp_i128_34>();
    world.register::<Comp_i128_35>();
    world.register::<Comp_i128_36>();
    world.register::<Comp_i128_37>();
    world.register::<Comp_i128_38>();
    world.register::<Comp_i128_39>();
    world.register::<Comp_i128_40>();
    world.register::<Comp_i128_41>();
    world.register::<Comp_i128_42>();
    world.register::<Comp_i128_43>();
    world.register::<Comp_i128_44>();
    world.register::<Comp_i128_45>();
    world.register::<Comp_i128_46>();
    world.register::<Comp_i128_47>();
    world.register::<Comp_i128_48>();
    world.register::<Comp_i128_49>();
    world.register::<Comp_i128_50>();
    world.register::<Comp_i128_51>();
    world.register::<Comp_i128_52>();
    world.register::<Comp_i128_53>();
    world.register::<Comp_i128_54>();
    world.register::<Comp_i128_55>();
    world.register::<Comp_i128_56>();
    world.register::<Comp_i128_57>();
    world.register::<Comp_i128_58>();
    world.register::<Comp_i128_59>();
    world.register::<Comp_i128_60>();
    world.register::<Comp_i128_61>();
    world.register::<Comp_i128_62>();
    world.register::<Comp_i128_63>();
    world.register::<Comp_i128_64>();
    world.register::<Comp_i128_65>();
    world.register::<Comp_i128_66>();
    world.register::<Comp_i128_67>();
    world.register::<Comp_i128_68>();
    world.register::<Comp_i128_69>();
    world.register::<Comp_i128_70>();
    world.register::<Comp_i128_71>();
    world.register::<Comp_i128_72>();
    world.register::<Comp_i128_73>();
    world.register::<Comp_i128_74>();
    world.register::<Comp_i128_75>();
    world.register::<Comp_i128_76>();
    world.register::<Comp_i128_77>();
    world.register::<Comp_i128_78>();
    world.register::<Comp_i128_79>();
    world.register::<Comp_i128_80>();
    world.register::<Comp_i128_81>();
    world.register::<Comp_i128_82>();
    world.register::<Comp_i128_83>();
    world.register::<Comp_i128_84>();
    world.register::<Comp_i128_85>();
    world.register::<Comp_i128_86>();
    world.register::<Comp_i128_87>();
    world.register::<Comp_i128_88>();
    world.register::<Comp_i128_89>();
    world.register::<Comp_i128_90>();
    world.register::<Comp_i128_91>();
    world.register::<Comp_i128_92>();
    world.register::<Comp_i128_93>();
    world.register::<Comp_i128_94>();
    world.register::<Comp_i128_95>();
    world.register::<Comp_i128_96>();
    world.register::<Comp_i128_97>();
    world.register::<Comp_i128_98>();
    world.register::<Comp_i128_99>();
    world.register::<Comp_i128_100>();
    world.register::<Comp_i128_101>();
    world.register::<Comp_i128_102>();
    world.register::<Comp_i128_103>();
    world.register::<Comp_i128_104>();
    world.register::<Comp_i128_105>();
    world.register::<Comp_i128_106>();
    world.register::<Comp_i128_107>();
    world.register::<Comp_i128_108>();
    world.register::<Comp_i128_109>();
    world.register::<Comp_i128_110>();
    world.register::<Comp_i128_111>();
    world.register::<Comp_i128_112>();
    world.register::<Comp_i128_113>();
    world.register::<Comp_i128_114>();
    world.register::<Comp_i128_115>();
    world.register::<Comp_i128_116>();
    world.register::<Comp_i128_117>();
    world.register::<Comp_i128_118>();
    world.register::<Comp_i128_119>();
    world.register::<Comp_i128_120>();
    world.register::<Comp_i128_121>();
    world.register::<Comp_i128_122>();
    world.register::<Comp_i128_123>();
    world.register::<Comp_i128_124>();
    world.register::<Comp_i128_125>();
    world.register::<Comp_i128_126>();
    world.register::<Comp_i128_127>();

    return Ok(())
}

//Add entities to the world
pub fn setup_entity(entity_size: i32, entity_count: i32, world: &mut World)->io::Result<()> {

    match entity_size {
        128 => {
            for _ in 0..entity_count {
                world.create_entity()
                    .with(Comp_i64_0(criterion::black_box(5)))
                    .with(Comp_i64_1(criterion::black_box(5)))
                    .build();
            }
        }
        256 => {
            for _ in 0..entity_count {
                world.create_entity()
                    .with(Comp_i128_0(criterion::black_box(5)))
                    .with(Comp_i128_1(criterion::black_box(5)))
                    .build();
            }
        }

        512 => {
            for _ in 0..entity_count {
                world.create_entity()
                    .with(Comp_i128_0(criterion::black_box(5)))
                    .with(Comp_i128_1(criterion::black_box(5)))
                    .with(Comp_i128_2(criterion::black_box(5)))
                    .with(Comp_i128_3(criterion::black_box(5)))
                    .build();
            }
        }

        1024 => {
            for _ in 0..entity_count {
                world.create_entity()
                    .with(Comp_i128_0(criterion::black_box(5)))
                    .with(Comp_i128_1(criterion::black_box(5)))
                    .with(Comp_i128_2(criterion::black_box(5)))
                    .with(Comp_i128_3(criterion::black_box(5)))
                    .with(Comp_i128_4(criterion::black_box(5)))
                    .with(Comp_i128_5(criterion::black_box(5)))
                    .with(Comp_i128_6(criterion::black_box(5)))
                    .with(Comp_i128_7(criterion::black_box(5)))
                    .build();
            }
        }

        2048 => {
            for _ in 0..entity_count {
                world.create_entity()
                    .with(Comp_i128_0(criterion::black_box(5)))
                    .with(Comp_i128_1(criterion::black_box(5)))
                    .with(Comp_i128_2(criterion::black_box(5)))
                    .with(Comp_i128_3(criterion::black_box(5)))
                    .with(Comp_i128_4(criterion::black_box(5)))
                    .with(Comp_i128_5(criterion::black_box(5)))
                    .with(Comp_i128_6(criterion::black_box(5)))
                    .with(Comp_i128_7(criterion::black_box(5)))
                    .with(Comp_i128_8(criterion::black_box(5)))
                    .with(Comp_i128_9(criterion::black_box(5)))
                    .with(Comp_i128_10(criterion::black_box(5)))
                    .with(Comp_i128_11(criterion::black_box(5)))
                    .with(Comp_i128_12(criterion::black_box(5)))
                    .with(Comp_i128_13(criterion::black_box(5)))
                    .with(Comp_i128_14(criterion::black_box(5)))
                    .with(Comp_i128_15(criterion::black_box(5)))
                    .build();
            }
        }

        4096 => {
            for _ in 0..entity_count {
                world.create_entity()
                    .with(Comp_i128_0(criterion::black_box(5)))
                    .with(Comp_i128_1(criterion::black_box(5)))
                    .with(Comp_i128_2(criterion::black_box(5)))
                    .with(Comp_i128_3(criterion::black_box(5)))
                    .with(Comp_i128_4(criterion::black_box(5)))
                    .with(Comp_i128_5(criterion::black_box(5)))
                    .with(Comp_i128_6(criterion::black_box(5)))
                    .with(Comp_i128_7(criterion::black_box(5)))
                    .with(Comp_i128_8(criterion::black_box(5)))
                    .with(Comp_i128_9(criterion::black_box(5)))
                    .with(Comp_i128_10(criterion::black_box(5)))
                    .with(Comp_i128_11(criterion::black_box(5)))
                    .with(Comp_i128_12(criterion::black_box(5)))
                    .with(Comp_i128_13(criterion::black_box(5)))
                    .with(Comp_i128_14(criterion::black_box(5)))
                    .with(Comp_i128_15(criterion::black_box(5)))
                    .with(Comp_i128_16(criterion::black_box(5)))
                    .with(Comp_i128_17(criterion::black_box(5)))
                    .with(Comp_i128_18(criterion::black_box(5)))
                    .with(Comp_i128_19(criterion::black_box(5)))
                    .with(Comp_i128_20(criterion::black_box(5)))
                    .with(Comp_i128_21(criterion::black_box(5)))
                    .with(Comp_i128_22(criterion::black_box(5)))
                    .with(Comp_i128_23(criterion::black_box(5)))
                    .with(Comp_i128_24(criterion::black_box(5)))
                    .with(Comp_i128_25(criterion::black_box(5)))
                    .with(Comp_i128_26(criterion::black_box(5)))
                    .with(Comp_i128_27(criterion::black_box(5)))
                    .with(Comp_i128_28(criterion::black_box(5)))
                    .with(Comp_i128_29(criterion::black_box(5)))
                    .with(Comp_i128_30(criterion::black_box(5)))
                    .with(Comp_i128_31(criterion::black_box(5)))
                    .build();
            }

        }

        8192 => {
            for _ in 0..entity_count {
                world.create_entity()
                    .with(Comp_i128_0(criterion::black_box(5)))
                    .with(Comp_i128_1(criterion::black_box(5)))
                    .with(Comp_i128_2(criterion::black_box(5)))
                    .with(Comp_i128_3(criterion::black_box(5)))
                    .with(Comp_i128_4(criterion::black_box(5)))
                    .with(Comp_i128_5(criterion::black_box(5)))
                    .with(Comp_i128_6(criterion::black_box(5)))
                    .with(Comp_i128_7(criterion::black_box(5)))
                    .with(Comp_i128_8(criterion::black_box(5)))
                    .with(Comp_i128_9(criterion::black_box(5)))
                    .with(Comp_i128_10(criterion::black_box(5)))
                    .with(Comp_i128_11(criterion::black_box(5)))
                    .with(Comp_i128_12(criterion::black_box(5)))
                    .with(Comp_i128_13(criterion::black_box(5)))
                    .with(Comp_i128_14(criterion::black_box(5)))
                    .with(Comp_i128_15(criterion::black_box(5)))
                    .with(Comp_i128_16(criterion::black_box(5)))
                    .with(Comp_i128_17(criterion::black_box(5)))
                    .with(Comp_i128_18(criterion::black_box(5)))
                    .with(Comp_i128_19(criterion::black_box(5)))
                    .with(Comp_i128_20(criterion::black_box(5)))
                    .with(Comp_i128_21(criterion::black_box(5)))
                    .with(Comp_i128_22(criterion::black_box(5)))
                    .with(Comp_i128_23(criterion::black_box(5)))
                    .with(Comp_i128_24(criterion::black_box(5)))
                    .with(Comp_i128_25(criterion::black_box(5)))
                    .with(Comp_i128_26(criterion::black_box(5)))
                    .with(Comp_i128_27(criterion::black_box(5)))
                    .with(Comp_i128_28(criterion::black_box(5)))
                    .with(Comp_i128_29(criterion::black_box(5)))
                    .with(Comp_i128_30(criterion::black_box(5)))
                    .with(Comp_i128_31(criterion::black_box(5)))
                    .with(Comp_i128_32(criterion::black_box(5)))
                    .with(Comp_i128_33(criterion::black_box(5)))
                    .with(Comp_i128_34(criterion::black_box(5)))
                    .with(Comp_i128_35(criterion::black_box(5)))
                    .with(Comp_i128_36(criterion::black_box(5)))
                    .with(Comp_i128_37(criterion::black_box(5)))
                    .with(Comp_i128_38(criterion::black_box(5)))
                    .with(Comp_i128_39(criterion::black_box(5)))
                    .with(Comp_i128_40(criterion::black_box(5)))
                    .with(Comp_i128_41(criterion::black_box(5)))
                    .with(Comp_i128_42(criterion::black_box(5)))
                    .with(Comp_i128_43(criterion::black_box(5)))
                    .with(Comp_i128_44(criterion::black_box(5)))
                    .with(Comp_i128_45(criterion::black_box(5)))
                    .with(Comp_i128_46(criterion::black_box(5)))
                    .with(Comp_i128_47(criterion::black_box(5)))
                    .with(Comp_i128_48(criterion::black_box(5)))
                    .with(Comp_i128_49(criterion::black_box(5)))
                    .with(Comp_i128_50(criterion::black_box(5)))
                    .with(Comp_i128_51(criterion::black_box(5)))
                    .with(Comp_i128_52(criterion::black_box(5)))
                    .with(Comp_i128_53(criterion::black_box(5)))
                    .with(Comp_i128_54(criterion::black_box(5)))
                    .with(Comp_i128_55(criterion::black_box(5)))
                    .with(Comp_i128_56(criterion::black_box(5)))
                    .with(Comp_i128_57(criterion::black_box(5)))
                    .with(Comp_i128_58(criterion::black_box(5)))
                    .with(Comp_i128_59(criterion::black_box(5)))
                    .with(Comp_i128_60(criterion::black_box(5)))
                    .with(Comp_i128_61(criterion::black_box(5)))
                    .with(Comp_i128_62(criterion::black_box(5)))
                    .with(Comp_i128_63(criterion::black_box(5)))
                    .build();
            }
        }

        16384 => {
            for _ in 0..entity_count {
                world.create_entity()
                    .with(Comp_i128_0(criterion::black_box(5)))
                    .with(Comp_i128_1(criterion::black_box(5)))
                    .with(Comp_i128_2(criterion::black_box(5)))
                    .with(Comp_i128_3(criterion::black_box(5)))
                    .with(Comp_i128_4(criterion::black_box(5)))
                    .with(Comp_i128_5(criterion::black_box(5)))
                    .with(Comp_i128_6(criterion::black_box(5)))
                    .with(Comp_i128_7(criterion::black_box(5)))
                    .with(Comp_i128_8(criterion::black_box(5)))
                    .with(Comp_i128_9(criterion::black_box(5)))
                    .with(Comp_i128_10(criterion::black_box(5)))
                    .with(Comp_i128_11(criterion::black_box(5)))
                    .with(Comp_i128_12(criterion::black_box(5)))
                    .with(Comp_i128_13(criterion::black_box(5)))
                    .with(Comp_i128_14(criterion::black_box(5)))
                    .with(Comp_i128_15(criterion::black_box(5)))
                    .with(Comp_i128_16(criterion::black_box(5)))
                    .with(Comp_i128_17(criterion::black_box(5)))
                    .with(Comp_i128_18(criterion::black_box(5)))
                    .with(Comp_i128_19(criterion::black_box(5)))
                    .with(Comp_i128_20(criterion::black_box(5)))
                    .with(Comp_i128_21(criterion::black_box(5)))
                    .with(Comp_i128_22(criterion::black_box(5)))
                    .with(Comp_i128_23(criterion::black_box(5)))
                    .with(Comp_i128_24(criterion::black_box(5)))
                    .with(Comp_i128_25(criterion::black_box(5)))
                    .with(Comp_i128_26(criterion::black_box(5)))
                    .with(Comp_i128_27(criterion::black_box(5)))
                    .with(Comp_i128_28(criterion::black_box(5)))
                    .with(Comp_i128_29(criterion::black_box(5)))
                    .with(Comp_i128_30(criterion::black_box(5)))
                    .with(Comp_i128_31(criterion::black_box(5)))
                    .with(Comp_i128_32(criterion::black_box(5)))
                    .with(Comp_i128_33(criterion::black_box(5)))
                    .with(Comp_i128_34(criterion::black_box(5)))
                    .with(Comp_i128_35(criterion::black_box(5)))
                    .with(Comp_i128_36(criterion::black_box(5)))
                    .with(Comp_i128_37(criterion::black_box(5)))
                    .with(Comp_i128_38(criterion::black_box(5)))
                    .with(Comp_i128_39(criterion::black_box(5)))
                    .with(Comp_i128_40(criterion::black_box(5)))
                    .with(Comp_i128_41(criterion::black_box(5)))
                    .with(Comp_i128_42(criterion::black_box(5)))
                    .with(Comp_i128_43(criterion::black_box(5)))
                    .with(Comp_i128_44(criterion::black_box(5)))
                    .with(Comp_i128_45(criterion::black_box(5)))
                    .with(Comp_i128_46(criterion::black_box(5)))
                    .with(Comp_i128_47(criterion::black_box(5)))
                    .with(Comp_i128_48(criterion::black_box(5)))
                    .with(Comp_i128_49(criterion::black_box(5)))
                    .with(Comp_i128_50(criterion::black_box(5)))
                    .with(Comp_i128_51(criterion::black_box(5)))
                    .with(Comp_i128_52(criterion::black_box(5)))
                    .with(Comp_i128_53(criterion::black_box(5)))
                    .with(Comp_i128_54(criterion::black_box(5)))
                    .with(Comp_i128_55(criterion::black_box(5)))
                    .with(Comp_i128_56(criterion::black_box(5)))
                    .with(Comp_i128_57(criterion::black_box(5)))
                    .with(Comp_i128_58(criterion::black_box(5)))
                    .with(Comp_i128_59(criterion::black_box(5)))
                    .with(Comp_i128_60(criterion::black_box(5)))
                    .with(Comp_i128_61(criterion::black_box(5)))
                    .with(Comp_i128_62(criterion::black_box(5)))
                    .with(Comp_i128_63(criterion::black_box(5)))
                    .with(Comp_i128_64(criterion::black_box(5)))
                    .with(Comp_i128_65(criterion::black_box(5)))
                    .with(Comp_i128_66(criterion::black_box(5)))
                    .with(Comp_i128_67(criterion::black_box(5)))
                    .with(Comp_i128_68(criterion::black_box(5)))
                    .with(Comp_i128_69(criterion::black_box(5)))
                    .with(Comp_i128_70(criterion::black_box(5)))
                    .with(Comp_i128_71(criterion::black_box(5)))
                    .with(Comp_i128_72(criterion::black_box(5)))
                    .with(Comp_i128_73(criterion::black_box(5)))
                    .with(Comp_i128_74(criterion::black_box(5)))
                    .with(Comp_i128_75(criterion::black_box(5)))
                    .with(Comp_i128_76(criterion::black_box(5)))
                    .with(Comp_i128_77(criterion::black_box(5)))
                    .with(Comp_i128_78(criterion::black_box(5)))
                    .with(Comp_i128_79(criterion::black_box(5)))
                    .with(Comp_i128_80(criterion::black_box(5)))
                    .with(Comp_i128_81(criterion::black_box(5)))
                    .with(Comp_i128_82(criterion::black_box(5)))
                    .with(Comp_i128_83(criterion::black_box(5)))
                    .with(Comp_i128_84(criterion::black_box(5)))
                    .with(Comp_i128_85(criterion::black_box(5)))
                    .with(Comp_i128_86(criterion::black_box(5)))
                    .with(Comp_i128_87(criterion::black_box(5)))
                    .with(Comp_i128_88(criterion::black_box(5)))
                    .with(Comp_i128_89(criterion::black_box(5)))
                    .with(Comp_i128_90(criterion::black_box(5)))
                    .with(Comp_i128_91(criterion::black_box(5)))
                    .with(Comp_i128_92(criterion::black_box(5)))
                    .with(Comp_i128_93(criterion::black_box(5)))
                    .with(Comp_i128_94(criterion::black_box(5)))
                    .with(Comp_i128_95(criterion::black_box(5)))
                    .with(Comp_i128_96(criterion::black_box(5)))
                    .with(Comp_i128_97(criterion::black_box(5)))
                    .with(Comp_i128_98(criterion::black_box(5)))
                    .with(Comp_i128_99(criterion::black_box(5)))
                    .with(Comp_i128_100(criterion::black_box(5)))
                    .with(Comp_i128_101(criterion::black_box(5)))
                    .with(Comp_i128_102(criterion::black_box(5)))
                    .with(Comp_i128_103(criterion::black_box(5)))
                    .with(Comp_i128_104(criterion::black_box(5)))
                    .with(Comp_i128_105(criterion::black_box(5)))
                    .with(Comp_i128_106(criterion::black_box(5)))
                    .with(Comp_i128_107(criterion::black_box(5)))
                    .with(Comp_i128_108(criterion::black_box(5)))
                    .with(Comp_i128_109(criterion::black_box(5)))
                    .with(Comp_i128_110(criterion::black_box(5)))
                    .with(Comp_i128_111(criterion::black_box(5)))
                    .with(Comp_i128_112(criterion::black_box(5)))
                    .with(Comp_i128_113(criterion::black_box(5)))
                    .with(Comp_i128_114(criterion::black_box(5)))
                    .with(Comp_i128_115(criterion::black_box(5)))
                    .with(Comp_i128_116(criterion::black_box(5)))
                    .with(Comp_i128_117(criterion::black_box(5)))
                    .with(Comp_i128_118(criterion::black_box(5)))
                    .with(Comp_i128_119(criterion::black_box(5)))
                    .with(Comp_i128_120(criterion::black_box(5)))
                    .with(Comp_i128_121(criterion::black_box(5)))
                    .with(Comp_i128_122(criterion::black_box(5)))
                    .with(Comp_i128_123(criterion::black_box(5)))
                    .with(Comp_i128_124(criterion::black_box(5)))
                    .with(Comp_i128_125(criterion::black_box(5)))
                    .with(Comp_i128_126(criterion::black_box(5)))
                    .with(Comp_i128_127(criterion::black_box(5)))
                    .build();
            }
        }
        _ => {}
    }
    return Ok(())
}

//Add systems to the dispatcher, set up threadcount
pub fn setup_dispatcher<'a, 'b>(size: i32)->Dispatcher<'a, 'b> {

    let pool = Arc::from(rayon::ThreadPoolBuilder::new().num_threads(1).build().unwrap());

    match size {
        128 => {
            let dispatcher = DispatcherBuilder::new()
                .with(Sys_128bit_0, "sys", &[])
                .with_pool(pool)
                .build();
            return dispatcher;
        }

        _ => {
            let dispatcher = DispatcherBuilder::new()
                .with(Sys_256bit_0, "sys", &[])
                .with_pool(pool)
                .build();
            return dispatcher;
        }
    }
}