pub trait Exp3: Send {
    fn run(&mut self);
    fn new(val: i128)->Self;
}

pub struct Obj128(pub i64, pub i64);
impl Exp3 for Obj128 {
    fn run(&mut self) {
        self.0 += self.1;
    }
    fn new(val: i128)->Self {
        let val= val as i64;
        return Obj128(val,val);
    }
}

pub struct Obj256(pub i128, pub i128);
impl Exp3 for Obj256 {
    fn run(&mut self) {
        self.0 += self.1;
    }
    fn new(val: i128)->Self {
        return Obj256(val,val);
    }
}

pub struct Obj512(pub i128, pub i128, pub i128, pub i128);
impl Exp3 for Obj512 {
    fn run(&mut self) {
        self.0 += self.3;
    }
    fn new(val: i128)->Self {
        return Obj512(val,val,val,val);
    }
}

pub struct Obj1024(pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128);
impl Exp3 for Obj1024 {
    fn run(&mut self) {
        self.0 += self.7;
    }
    fn new(val: i128)->Self{
        return Obj1024(val,val,val,val,val,val,val,val);
    }
}

pub struct Obj2048(pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128,
                   pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128);
impl Exp3 for Obj2048 {
    fn run(&mut self) {self.0 += self.15; }
    fn new(val: i128)->Self {
        return Obj2048(val,val,val,val,val,val,val,val,val,val,val,val,val,val,val,val);
    }
}

pub struct Obj4096(pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128,
                   pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128,
                   pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128,
                   pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128);
impl Exp3 for Obj4096 {
    fn run(&mut self) {self.0 += self.31; }
    fn new(val:i128)-> Self {
        return Obj4096(val,val,val,val,val,val,val,val,val,val,val,val,val,val,val,val,
                       val,val,val,val,val,val,val,val,val,val,val,val,val,val,val,val);
    }
}

pub struct Obj8192(pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128,
                   pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128,
                   pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128,
                   pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128,
                   pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128,
                   pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128,
                   pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128,
                   pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128);
impl Exp3 for Obj8192 {
    fn run(&mut self) {self.0 += self.63; }
    fn new(val:i128)-> Self {
        return Obj8192(val,val,val,val,val,val,val,val,val,val,val,val,val,val,val,val,
                       val,val,val,val,val,val,val,val,val,val,val,val,val,val,val,val,
                       val,val,val,val,val,val,val,val,val,val,val,val,val,val,val,val,
                       val,val,val,val,val,val,val,val,val,val,val,val,val,val,val,val);
    }
}

pub struct Obj16384(pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128,
                    pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128,
                    pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128,
                    pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128,
                    pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128,
                    pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128,
                    pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128,
                    pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128,
                    pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128,
                    pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128,
                    pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128,
                    pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128,
                    pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128,
                    pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128,
                    pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128,
                    pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128);
impl Exp3 for Obj16384 {
    fn run(&mut self) { self.0 += self.127; }
    fn new(val: i128) -> Self {
        return Obj16384(val, val, val, val, val, val, val, val, val, val, val, val, val, val, val, val,
                        val, val, val, val, val, val, val, val, val, val, val, val, val, val, val, val,
                        val, val, val, val, val, val, val, val, val, val, val, val, val, val, val, val,
                        val, val, val, val, val, val, val, val, val, val, val, val, val, val, val, val,
                        val, val, val, val, val, val, val, val, val, val, val, val, val, val, val, val,
                        val, val, val, val, val, val, val, val, val, val, val, val, val, val, val, val,
                        val, val, val, val, val, val, val, val, val, val, val, val, val, val, val, val,
                        val, val, val, val, val, val, val, val, val, val, val, val, val, val, val, val);
    }
}
