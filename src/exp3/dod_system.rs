use specs::prelude::*;
use specs::Join;
use super::dod_component::*;

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Sys_128bit_0;
impl<'a> System<'a> for Sys_128bit_0 {

    type SystemData = (WriteStorage<'a, Comp_i64_0>, ReadStorage<'a, Comp_i64_1>);

    fn run(&mut self, (mut x, y): Self::SystemData) {
        for (x, y) in (&mut x, &y).join() {
            x.0 += y.0;
        }
    }
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Sys_256bit_0;
impl<'a> System<'a> for Sys_256bit_0 {

    type SystemData = (WriteStorage<'a, Comp_i128_0>, ReadStorage<'a, Comp_i128_1>);

    fn run(&mut self, (mut x, y): Self::SystemData) {
        for (x, y) in (&mut x, &y).join() {
            x.0 += y.0;
        }
    }
}