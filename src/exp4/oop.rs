use super::oop_obj::*;
use std::sync::{Arc, RwLock};
use rayon::ThreadPoolBuilder;
use rayon::iter::IntoParallelRefMutIterator;

type ThreadPoolWrapper = Option<::std::sync::Arc<::rayon::ThreadPool>>;

pub fn obj_setup<T: Exp4>(entity_count: i32) -> Vec<T> {

    let mut vec: Vec<T> = Vec::new();
    for _ in 0..entity_count {
        let tmp = T::new(criterion::black_box(5));
        vec.push(tmp);
    }

    return vec;
}

pub struct OOPWorld<T: Exp4> {
    stages: Vec<Stage<T>>,
    pool: Arc<RwLock<ThreadPoolWrapper>>,
    count: usize
}

impl <T: Exp4> OOPWorld <T> {
    pub fn new(vec: Vec<T>, thread_count: usize)->OOPWorld<T>{
        let pool: ThreadPoolWrapper = Some(Arc::from(ThreadPoolBuilder::new().num_threads(thread_count).build().unwrap()));
        let pool: Arc<RwLock<ThreadPoolWrapper>> = Arc::from(RwLock::from(pool));

        let stage: Stage<T> = Stage::new(vec);
        let mut stages: Vec<Stage<T>> = Vec::new();
        stages.push(stage);

        return OOPWorld{
            stages,
            pool,
            count: thread_count
        };
    }

    pub fn execute(&mut self){
        let stages = &mut self.stages;
        let count = self.count.clone();
        self.pool
            .read()
            .unwrap()
            .as_ref()
            .unwrap()
            .install(move || {
                for stage in stages {
                    stage.execute(count);
                }
            });
    }
}

struct Stage<T: Exp4> {
    groups: Vec<Vec<T>>
}

impl <T: Exp4> Stage <T> {
    fn new(vec: Vec<T>)-> Stage<T> {

        let mut groups: Vec<Vec<T>> = Vec::new();
        groups.push(vec);

        return Stage {
            groups
        };
    }

    fn execute(&mut self, count: usize) {
        use rayon::iter::ParallelIterator;
        self.groups.par_iter_mut().for_each(|group| {
            for obj in group {
                match count {
                    1 => {
                        obj.run0();
                    },
                    2 => {
                        obj.run0();
                        obj.run1();
                    },
                    3 => {
                        obj.run0();
                        obj.run1();
                        obj.run2();
                    },
                    4 => {
                        obj.run0();
                        obj.run1();
                        obj.run2();
                        obj.run3();
                    },
                    5 => {
                        obj.run0();
                        obj.run1();
                        obj.run2();
                        obj.run3();
                        obj.run4();
                    },
                    6 => {
                        obj.run0();
                        obj.run1();
                        obj.run2();
                        obj.run3();
                        obj.run4();
                        obj.run5();
                    },
                    7 => {
                        obj.run0();
                        obj.run1();
                        obj.run2();
                        obj.run3();
                        obj.run4();
                        obj.run5();
                        obj.run6();
                    },
                    8 => {
                        obj.run0();
                        obj.run1();
                        obj.run2();
                        obj.run3();
                        obj.run4();
                        obj.run5();
                        obj.run6();
                        obj.run7();
                    },
                    9 => {
                        obj.run0();
                        obj.run1();
                        obj.run2();
                        obj.run3();
                        obj.run4();
                        obj.run5();
                        obj.run6();
                        obj.run7();
                        obj.run8();
                    },
                    10 => {
                        obj.run0();
                        obj.run1();
                        obj.run2();
                        obj.run3();
                        obj.run4();
                        obj.run5();
                        obj.run6();
                        obj.run7();
                        obj.run8();
                        obj.run9();
                    },
                    11 => {
                        obj.run0();
                        obj.run1();
                        obj.run2();
                        obj.run3();
                        obj.run4();
                        obj.run5();
                        obj.run6();
                        obj.run7();
                        obj.run8();
                        obj.run9();
                        obj.run10();
                    },
                    12 => {
                        obj.run0();
                        obj.run1();
                        obj.run2();
                        obj.run3();
                        obj.run4();
                        obj.run5();
                        obj.run6();
                        obj.run7();
                        obj.run8();
                        obj.run9();
                        obj.run10();
                        obj.run11();
                    },
                    13 => {
                        obj.run0();
                        obj.run1();
                        obj.run2();
                        obj.run3();
                        obj.run4();
                        obj.run5();
                        obj.run6();
                        obj.run7();
                        obj.run8();
                        obj.run9();
                        obj.run10();
                        obj.run11();
                        obj.run12();
                    },
                    14 => {
                        obj.run0();
                        obj.run1();
                        obj.run2();
                        obj.run3();
                        obj.run4();
                        obj.run5();
                        obj.run6();
                        obj.run7();
                        obj.run8();
                        obj.run9();
                        obj.run10();
                        obj.run11();
                        obj.run12();
                        obj.run13();
                    },
                    15 => {
                        obj.run0();
                        obj.run1();
                        obj.run2();
                        obj.run3();
                        obj.run4();
                        obj.run5();
                        obj.run6();
                        obj.run7();
                        obj.run8();
                        obj.run9();
                        obj.run10();
                        obj.run11();
                        obj.run12();
                        obj.run13();
                        obj.run14();
                    }
                    _ => {panic!("unexpected thread_count");}
                }
            }
        })
    }
}

