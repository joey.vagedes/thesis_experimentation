use specs::prelude::*;
use super::dod_component::*;

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Sys_128bit_0;
impl<'a> System<'a> for Sys_128bit_0 {

    type SystemData = (WriteStorage<'a, Comp_i128_0>);

    fn run(&mut self, mut x: Self::SystemData) {
        for x in (&mut x).join() {
            x.0 += x.0;
        }
    }
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Sys_256bit_0;
impl<'a> System<'a> for Sys_256bit_0 {

    type SystemData = (WriteStorage<'a, Comp_i128_0>, ReadStorage<'a, Comp_i128_1>);

    fn run(&mut self, (mut x, y): Self::SystemData) {
        for (x, y) in (&mut x, &y).join() {
            x.0 += y.0;
        }
    }
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Sys_256bit_1;
impl<'a> System<'a> for Sys_256bit_1 {

    type SystemData = (WriteStorage<'a, Comp_i128_2>, ReadStorage<'a, Comp_i128_1>);

    fn run(&mut self, (mut x, y): Self::SystemData) {
        for (x, y) in (&mut x, &y).join() {
            x.0 += y.0;
        }
    }
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Sys_256bit_2;
impl<'a> System<'a> for Sys_256bit_2 {

    type SystemData = (WriteStorage<'a, Comp_i128_3>, ReadStorage<'a, Comp_i128_1>);

    fn run(&mut self, (mut x, y): Self::SystemData) {
        for (x, y) in (&mut x, &y).join() {
            x.0 += y.0
        }
    }
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Sys_256bit_3;
impl<'a> System<'a> for Sys_256bit_3 {

    type SystemData = (WriteStorage<'a, Comp_i128_4>, ReadStorage<'a, Comp_i128_1>);

    fn run(&mut self, (mut x, y): Self::SystemData) {
        for (x, y) in (&mut x, &y).join() {
            x.0 += y.0;
        }
    }
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Sys_256bit_4;
impl<'a> System<'a> for Sys_256bit_4 {

    type SystemData = (WriteStorage<'a, Comp_i128_5>, ReadStorage<'a, Comp_i128_1>);

    fn run(&mut self, (mut x, y): Self::SystemData) {
        for (x, y) in (&mut x, &y).join() {
            x.0 += y.0;
        }
    }
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Sys_256bit_5;
impl<'a> System<'a> for Sys_256bit_5 {

    type SystemData = (WriteStorage<'a, Comp_i128_6>, ReadStorage<'a, Comp_i128_1>);

    fn run(&mut self, (mut x, y): Self::SystemData) {
        for (x, y) in (&mut x, &y).join() {
            x.0 += y.0;
        }
    }
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Sys_256bit_6;
impl<'a> System<'a> for Sys_256bit_6 {

    type SystemData = (WriteStorage<'a, Comp_i128_7>, ReadStorage<'a, Comp_i128_1>);

    fn run(&mut self, (mut x, y): Self::SystemData) {
        for (x, y) in (&mut x, &y).join() {
            x.0 += y.0;
        }
    }
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Sys_256bit_7;
impl<'a> System<'a> for Sys_256bit_7 {

    type SystemData = (WriteStorage<'a, Comp_i128_8>, ReadStorage<'a, Comp_i128_1>);

    fn run(&mut self, (mut x, y): Self::SystemData) {
        for (x, y) in (&mut x, &y).join() {
            x.0 += y.0;
        }
    }
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Sys_256bit_8;
impl<'a> System<'a> for Sys_256bit_8 {

    type SystemData = (WriteStorage<'a, Comp_i128_9>, ReadStorage<'a, Comp_i128_1>);

    fn run(&mut self, (mut x, y): Self::SystemData) {
        for (x, y) in (&mut x, &y).join() {
            x.0 += y.0;
        }
    }
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Sys_256bit_9;
impl<'a> System<'a> for Sys_256bit_9 {

    type SystemData = (WriteStorage<'a, Comp_i128_10>, ReadStorage<'a, Comp_i128_1>);

    fn run(&mut self, (mut x, y): Self::SystemData) {
        for (x, y) in (&mut x, &y).join() {
            x.0 += y.0;
        }
    }
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Sys_256bit_10;
impl<'a> System<'a> for Sys_256bit_10 {

    type SystemData = (WriteStorage<'a, Comp_i128_11>, ReadStorage<'a, Comp_i128_1>);

    fn run(&mut self, (mut x, y): Self::SystemData) {
        for (x, y) in (&mut x, &y).join() {
            x.0 += y.0;
        }
    }
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Sys_256bit_11;
impl<'a> System<'a> for Sys_256bit_11 {

    type SystemData = (WriteStorage<'a, Comp_i128_12>, ReadStorage<'a, Comp_i128_1>);

    fn run(&mut self, (mut x, y): Self::SystemData) {
        for (x, y) in (&mut x, &y).join() {
            x.0 += y.0;
        }
    }
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Sys_256bit_12;
impl<'a> System<'a> for Sys_256bit_12 {

    type SystemData = (WriteStorage<'a, Comp_i128_13>, ReadStorage<'a, Comp_i128_1>);

    fn run(&mut self, (mut x, y): Self::SystemData) {
        for (x, y) in (&mut x, &y).join() {
            x.0 += y.0;
        }
    }
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Sys_256bit_13;
impl<'a> System<'a> for Sys_256bit_13 {

    type SystemData = (WriteStorage<'a, Comp_i128_14>, ReadStorage<'a, Comp_i128_1>);

    fn run(&mut self, (mut x, y): Self::SystemData) {
        for (x, y) in (&mut x, &y).join() {
            x.0 += y.0;
        }
    }
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Sys_256bit_14;
impl<'a> System<'a> for Sys_256bit_14 {

    type SystemData = (WriteStorage<'a, Comp_i128_15>, ReadStorage<'a, Comp_i128_1>);

    fn run(&mut self, (mut x, y): Self::SystemData) {
        for (x, y) in (&mut x, &y).join() {
            x.0 += y.0;
        }
    }
}