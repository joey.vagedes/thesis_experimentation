use specs::prelude::*;
use std::io;
use super::dod_obj::*;
use std::sync::Arc;

//All Entities use 2048 bits, which is 16 i128's
pub fn setup_component(world: &mut World)-> io::Result<()> {
    world.register::<Comp_i128_0>();
    world.register::<Comp_i128_1>();
    world.register::<Comp_i128_2>();
    world.register::<Comp_i128_3>();
    world.register::<Comp_i128_4>();
    world.register::<Comp_i128_5>();
    world.register::<Comp_i128_6>();
    world.register::<Comp_i128_7>();
    world.register::<Comp_i128_8>();
    world.register::<Comp_i128_9>();
    world.register::<Comp_i128_10>();
    world.register::<Comp_i128_11>();
    world.register::<Comp_i128_12>();
    world.register::<Comp_i128_13>();
    world.register::<Comp_i128_14>();
    world.register::<Comp_i128_15>();

    return Ok(())
}

//All Entities use 2048 bits, which is 16 i128's
pub fn setup_entity(world: &mut World)->io::Result<()> {

    for _ in 0..5000 {
        world.create_entity()
            .with(Comp_i128_0(criterion::black_box(5)))
            .with(Comp_i128_1(criterion::black_box(5)))
            .with(Comp_i128_2(criterion::black_box(5)))
            .with(Comp_i128_3(criterion::black_box(5)))
            .with(Comp_i128_4(criterion::black_box(5)))
            .with(Comp_i128_5(criterion::black_box(5)))
            .with(Comp_i128_6(criterion::black_box(5)))
            .with(Comp_i128_7(criterion::black_box(5)))
            .with(Comp_i128_8(criterion::black_box(5)))
            .with(Comp_i128_9(criterion::black_box(5)))
            .with(Comp_i128_10(criterion::black_box(5)))
            .with(Comp_i128_11(criterion::black_box(5)))
            .with(Comp_i128_12(criterion::black_box(5)))
            .with(Comp_i128_13(criterion::black_box(5)))
            .with(Comp_i128_14(criterion::black_box(5)))
            .with(Comp_i128_15(criterion::black_box(5)))
            .build();
    }
    return Ok(())
}

//This differs based on which experiment is going on
pub fn setup_dispatcher<'a, 'b>(size: i32)->Dispatcher<'a, 'b> {

    let pool = Arc::from(rayon::ThreadPoolBuilder::new().num_threads(1).build().unwrap());

    match size {
        2048 => {
            let dispatcher = DispatcherBuilder::new()
                .with(Sys_2048, "sys", &[])
                .with_pool(pool)
                .build();
            return dispatcher;
        }

        1912 => {
            let dispatcher = DispatcherBuilder::new()
                .with(Sys_1912, "sys", &[])
                .with_pool(pool)
                .build();
            return dispatcher;
        }

        1792 => {
            let dispatcher = DispatcherBuilder::new()
                .with(Sys_1792, "sys", &[])
                .with_pool(pool)
                .build();
            return dispatcher;
        }

        1664 => {
            let dispatcher = DispatcherBuilder::new()
                .with(Sys_1664, "sys", &[])
                .with_pool(pool)
                .build();
            return dispatcher;
        }

        1536 => {
            let dispatcher = DispatcherBuilder::new()
                .with(Sys_1536, "sys", &[])
                .with_pool(pool)
                .build();
            return dispatcher;
        }

        1408 => {
            let dispatcher = DispatcherBuilder::new()
                .with(Sys_1408, "sys", &[])
                .with_pool(pool)
                .build();
            return dispatcher;
        }

        1280 => {
            let dispatcher = DispatcherBuilder::new()
                .with(Sys_1280, "sys", &[])
                .with_pool(pool)
                .build();
            return dispatcher;
        }

        1152 => {
            let dispatcher = DispatcherBuilder::new()
                .with(Sys_1152, "sys", &[])
                .with_pool(pool)
                .build();
            return dispatcher;
        }

        1024 => {
            let dispatcher = DispatcherBuilder::new()
                .with(Sys_1024, "sys", &[])
                .with_pool(pool)
                .build();
            return dispatcher;
        }

        896 => {
            let dispatcher = DispatcherBuilder::new()
                .with(Sys_896, "sys", &[])
                .with_pool(pool)
                .build();
            return dispatcher;
        }

        768 => {
            let dispatcher = DispatcherBuilder::new()
                .with(Sys_768, "sys", &[])
                .with_pool(pool)
                .build();
            return dispatcher;
        }

        640 => {
            let dispatcher = DispatcherBuilder::new()
                .with(Sys_640, "sys", &[])
                .with_pool(pool)
                .build();
            return dispatcher;
        }

        512 => {
            let dispatcher = DispatcherBuilder::new()
                .with(Sys_512, "sys", &[])
                .with_pool(pool)
                .build();
            return dispatcher;
        }

        384 => {
            let dispatcher = DispatcherBuilder::new()
                .with(Sys_384, "sys", &[])
                .with_pool(pool)
                .build();
            return dispatcher;
        }

        256 => {
            let dispatcher = DispatcherBuilder::new()
                .with(Sys_256, "sys", &[])
                .with_pool(pool)
                .build();
            return dispatcher;
        }

        _ => {panic!("unknown data size");}
    }
}