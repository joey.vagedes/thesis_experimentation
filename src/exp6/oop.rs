use super::oop_obj::*;
use std::sync::{Arc, RwLock};
use rayon::*;
use rayon::iter::IntoParallelRefMutIterator;

type ThreadPoolWrapper = Option<::std::sync::Arc<::rayon::ThreadPool>>;

pub fn obj_setup<T: Exp6>()-> Vec<T> {

    let mut vec: Vec<T> = Vec::new();
    for _ in 0..5000 {
        let tmp = T::new(criterion::black_box(5));
        vec.push(tmp);
    }

    return vec;
}

//----------------------------------------------------------
pub struct OOPWorld<T: Exp6> {
    stages: Vec<Stage<T>>,
    pool: Arc<RwLock<ThreadPoolWrapper>>
}

impl <T: Exp6> OOPWorld <T> {
    pub fn new(vec: Vec<T>, )->OOPWorld<T>{
        let pool: ThreadPoolWrapper = Some(Arc::from(ThreadPoolBuilder::new().num_threads(1).build().unwrap()));
        let pool: Arc<RwLock<ThreadPoolWrapper>> = Arc::from(RwLock::from(pool));

        let stage: Stage<T> = Stage::new(vec);
        let mut stages: Vec<Stage<T>> = Vec::new();
        stages.push(stage);

        return OOPWorld{
            stages,
            pool
        };
    }

    pub fn execute(&mut self){
        let stages = &mut self.stages;

        self.pool
            .read()
            .unwrap()
            .as_ref()
            .unwrap()
            .install(move || {
                for stage in stages {
                    stage.execute();
                }
            });
    }
}

//----------------------------------------------------------

struct Stage<T: Exp6> {
    groups: Vec<Vec<T>>
}

impl <T: Exp6> Stage <T> {
    fn new(vec: Vec<T>)-> Stage<T> {

        let mut groups: Vec<Vec<T>> = Vec::new();
        groups.push(vec);

        return Stage {
            groups
        };
    }

    fn execute(&mut self) {
        use rayon::iter::ParallelIterator;
        self.groups.par_iter_mut().for_each(|group| {
            for obj in group {
                obj.run();
            }
        })
    }
}

