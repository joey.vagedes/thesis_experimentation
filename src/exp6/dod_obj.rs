use specs::prelude::*;

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_0(pub i128);
impl Component for Comp_i128_0 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_1(pub i128);
impl Component for Comp_i128_1 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_2(pub i128);
impl Component for Comp_i128_2 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_3(pub i128);
impl Component for Comp_i128_3 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_4(pub i128);
impl Component for Comp_i128_4 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_5(pub i128);
impl Component for Comp_i128_5 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_6(pub i128);
impl Component for Comp_i128_6 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_7(pub i128);
impl Component for Comp_i128_7 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_8(pub i128);
impl Component for Comp_i128_8 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_9(pub i128);
impl Component for Comp_i128_9 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_10(pub i128);
impl Component for Comp_i128_10 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_11(pub i128);
impl Component for Comp_i128_11 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_12(pub i128);
impl Component for Comp_i128_12 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_13(pub i128);
impl Component for Comp_i128_13 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_14(pub i128);
impl Component for Comp_i128_14 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_15(pub i128);
impl Component for Comp_i128_15 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Sys_2048;
impl<'a> System<'a> for Sys_2048 {

    type SystemData = (WriteStorage<'a, Comp_i128_0>, ReadStorage<'a, Comp_i128_1>,
                       ReadStorage<'a, Comp_i128_2>, ReadStorage<'a, Comp_i128_3>,
                       ReadStorage<'a, Comp_i128_4>, ReadStorage<'a, Comp_i128_5>,
                       ReadStorage<'a, Comp_i128_6>, ReadStorage<'a, Comp_i128_7>,
                       ReadStorage<'a, Comp_i128_8>, ReadStorage<'a, Comp_i128_9>,
                       ReadStorage<'a, Comp_i128_10>, ReadStorage<'a, Comp_i128_11>,
                       ReadStorage<'a, Comp_i128_12>, ReadStorage<'a, Comp_i128_13>,
                       ReadStorage<'a, Comp_i128_14>, ReadStorage<'a, Comp_i128_15>);

    fn run(&mut self, (mut o0, o1, o2, o3, o4, o5, o6, o7, o8, o9, o10, o11, o12, o13, o14, o15): Self::SystemData) {
        // The `.join()` combines multiple component storages,
        // so we get access to all entities which have
        // both a position and a velocity.
        for (o0, o1, o2, o3, o4, o5, o6, o7, o8, o9, o10, o11, o12, o13, o14, o15) in
            (&mut o0, &o1, &o2, &o3, &o4, &o5, &o6, &o7, &o8, &o9, &o10, &o11, &o12, &o13, &o14, &o15).join() {
            o0.0 += o1.0 + o2.0 + o3.0 + o4.0 + o5.0 + o6.0 + o7.0 + o8.0 + o9.0 + o10.0 + o11.0 + o12.0 + o13.0 + o14.0 + o15.0;
        }
    }
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Sys_1912;
impl<'a> System<'a> for Sys_1912 {

    type SystemData = (WriteStorage<'a, Comp_i128_0>, ReadStorage<'a, Comp_i128_1>,
                       ReadStorage<'a, Comp_i128_2>, ReadStorage<'a, Comp_i128_3>,
                       ReadStorage<'a, Comp_i128_4>, ReadStorage<'a, Comp_i128_5>,
                       ReadStorage<'a, Comp_i128_6>, ReadStorage<'a, Comp_i128_7>,
                       ReadStorage<'a, Comp_i128_8>, ReadStorage<'a, Comp_i128_9>,
                       ReadStorage<'a, Comp_i128_10>, ReadStorage<'a, Comp_i128_11>,
                       ReadStorage<'a, Comp_i128_12>, ReadStorage<'a, Comp_i128_13>,
                       ReadStorage<'a, Comp_i128_14>);

    fn run(&mut self, (mut o0, o1, o2, o3, o4, o5, o6, o7, o8, o9, o10, o11, o12, o13, o14): Self::SystemData) {
        // The `.join()` combines multiple component storages,
        // so we get access to all entities which have
        // both a position and a velocity.
        for (o0, o1, o2, o3, o4, o5, o6, o7, o8, o9, o10, o11, o12, o13, o14) in
            (&mut o0, &o1, &o2, &o3, &o4, &o5, &o6, &o7, &o8, &o9, &o10, &o11, &o12, &o13, &o14).join() {
            o0.0 += o1.0 + o2.0 + o3.0 + o4.0 + o5.0 + o6.0 + o7.0 + o8.0 + o9.0 + o10.0 + o11.0 + o12.0 + o13.0 + o14.0;
        }
    }
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Sys_1792;
impl<'a> System<'a> for Sys_1792 {

    type SystemData = (WriteStorage<'a, Comp_i128_0>, ReadStorage<'a, Comp_i128_1>,
                       ReadStorage<'a, Comp_i128_2>, ReadStorage<'a, Comp_i128_3>,
                       ReadStorage<'a, Comp_i128_4>, ReadStorage<'a, Comp_i128_5>,
                       ReadStorage<'a, Comp_i128_6>, ReadStorage<'a, Comp_i128_7>,
                       ReadStorage<'a, Comp_i128_8>, ReadStorage<'a, Comp_i128_9>,
                       ReadStorage<'a, Comp_i128_10>, ReadStorage<'a, Comp_i128_11>,
                       ReadStorage<'a, Comp_i128_12>, ReadStorage<'a, Comp_i128_13>);

    fn run(&mut self, (mut o0, o1, o2, o3, o4, o5, o6, o7, o8, o9, o10, o11, o12, o13): Self::SystemData) {
        // The `.join()` combines multiple component storages,
        // so we get access to all entities which have
        // both a position and a velocity.
        for (o0, o1, o2, o3, o4, o5, o6, o7, o8, o9, o10, o11, o12, o13) in
            (&mut o0, &o1, &o2, &o3, &o4, &o5, &o6, &o7, &o8, &o9, &o10, &o11, &o12, &o13).join() {
            o0.0 += o1.0 + o2.0 + o3.0 + o4.0 + o5.0 + o6.0 + o7.0 + o8.0 + o9.0 + o10.0 + o11.0 + o12.0 + o13.0;
        }
    }
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Sys_1664;
impl<'a> System<'a> for Sys_1664 {

    type SystemData = (WriteStorage<'a, Comp_i128_0>, ReadStorage<'a, Comp_i128_1>,
                       ReadStorage<'a, Comp_i128_2>, ReadStorage<'a, Comp_i128_3>,
                       ReadStorage<'a, Comp_i128_4>, ReadStorage<'a, Comp_i128_5>,
                       ReadStorage<'a, Comp_i128_6>, ReadStorage<'a, Comp_i128_7>,
                       ReadStorage<'a, Comp_i128_8>, ReadStorage<'a, Comp_i128_9>,
                       ReadStorage<'a, Comp_i128_10>, ReadStorage<'a, Comp_i128_11>,
                       ReadStorage<'a, Comp_i128_12>);

    fn run(&mut self, (mut o0, o1, o2, o3, o4, o5, o6, o7, o8, o9, o10, o11, o12): Self::SystemData) {
        // The `.join()` combines multiple component storages,
        // so we get access to all entities which have
        // both a position and a velocity.
        for (o0, o1, o2, o3, o4, o5, o6, o7, o8, o9, o10, o11, o12) in
            (&mut o0, &o1, &o2, &o3, &o4, &o5, &o6, &o7, &o8, &o9, &o10, &o11, &o12).join() {
            o0.0 += o1.0 + o2.0 + o3.0 + o4.0 + o5.0 + o6.0 + o7.0 + o8.0 + o9.0 + o10.0 + o11.0 + o12.0;
        }
    }
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Sys_1536;
impl<'a> System<'a> for Sys_1536 {

    type SystemData = (WriteStorage<'a, Comp_i128_0>, ReadStorage<'a, Comp_i128_1>,
                       ReadStorage<'a, Comp_i128_2>, ReadStorage<'a, Comp_i128_3>,
                       ReadStorage<'a, Comp_i128_4>, ReadStorage<'a, Comp_i128_5>,
                       ReadStorage<'a, Comp_i128_6>, ReadStorage<'a, Comp_i128_7>,
                       ReadStorage<'a, Comp_i128_8>, ReadStorage<'a, Comp_i128_9>,
                       ReadStorage<'a, Comp_i128_10>, ReadStorage<'a, Comp_i128_11>);

    fn run(&mut self, (mut o0, o1, o2, o3, o4, o5, o6, o7, o8, o9, o10, o11): Self::SystemData) {
        // The `.join()` combines multiple component storages,
        // so we get access to all entities which have
        // both a position and a velocity.
        for (o0, o1, o2, o3, o4, o5, o6, o7, o8, o9, o10, o11) in
            (&mut o0, &o1, &o2, &o3, &o4, &o5, &o6, &o7, &o8, &o9, &o10, &o11).join() {
            o0.0 += o1.0 + o2.0 + o3.0 + o4.0 + o5.0 + o6.0 + o7.0 + o8.0 + o9.0 + o10.0 + o11.0;
        }
    }
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Sys_1408;
impl<'a> System<'a> for Sys_1408 {

    type SystemData = (WriteStorage<'a, Comp_i128_0>, ReadStorage<'a, Comp_i128_1>,
                       ReadStorage<'a, Comp_i128_2>, ReadStorage<'a, Comp_i128_3>,
                       ReadStorage<'a, Comp_i128_4>, ReadStorage<'a, Comp_i128_5>,
                       ReadStorage<'a, Comp_i128_6>, ReadStorage<'a, Comp_i128_7>,
                       ReadStorage<'a, Comp_i128_8>, ReadStorage<'a, Comp_i128_9>,
                       ReadStorage<'a, Comp_i128_10>);

    fn run(&mut self, (mut o0, o1, o2, o3, o4, o5, o6, o7, o8, o9, o10): Self::SystemData) {
        // The `.join()` combines multiple component storages,
        // so we get access to all entities which have
        // both a position and a velocity.
        for (o0, o1, o2, o3, o4, o5, o6, o7, o8, o9, o10) in
            (&mut o0, &o1, &o2, &o3, &o4, &o5, &o6, &o7, &o8, &o9, &o10).join() {
            o0.0 += o1.0 + o2.0 + o3.0 + o4.0 + o5.0 + o6.0 + o7.0 + o8.0 + o9.0 + o10.0;
        }
    }
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Sys_1280;
impl<'a> System<'a> for Sys_1280 {

    type SystemData = (WriteStorage<'a, Comp_i128_0>, ReadStorage<'a, Comp_i128_1>,
                       ReadStorage<'a, Comp_i128_2>, ReadStorage<'a, Comp_i128_3>,
                       ReadStorage<'a, Comp_i128_4>, ReadStorage<'a, Comp_i128_5>,
                       ReadStorage<'a, Comp_i128_6>, ReadStorage<'a, Comp_i128_7>,
                       ReadStorage<'a, Comp_i128_8>, ReadStorage<'a, Comp_i128_9>);

    fn run(&mut self, (mut o0, o1, o2, o3, o4, o5, o6, o7, o8, o9): Self::SystemData) {
        // The `.join()` combines multiple component storages,
        // so we get access to all entities which have
        // both a position and a velocity.
        for (o0, o1, o2, o3, o4, o5, o6, o7, o8, o9) in
            (&mut o0, &o1, &o2, &o3, &o4, &o5, &o6, &o7, &o8, &o9).join() {
            o0.0 += o1.0 + o2.0 + o3.0 + o4.0 + o5.0 + o6.0 + o7.0 + o8.0 + o9.0;
        }
    }
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Sys_1152;
impl<'a> System<'a> for Sys_1152 {

    type SystemData = (WriteStorage<'a, Comp_i128_0>, ReadStorage<'a, Comp_i128_1>,
                       ReadStorage<'a, Comp_i128_2>, ReadStorage<'a, Comp_i128_3>,
                       ReadStorage<'a, Comp_i128_4>, ReadStorage<'a, Comp_i128_5>,
                       ReadStorage<'a, Comp_i128_6>, ReadStorage<'a, Comp_i128_7>,
                       ReadStorage<'a, Comp_i128_8>);

    fn run(&mut self, (mut o0, o1, o2, o3, o4, o5, o6, o7, o8): Self::SystemData) {
        // The `.join()` combines multiple component storages,
        // so we get access to all entities which have
        // both a position and a velocity.
        for (o0, o1, o2, o3, o4, o5, o6, o7, o8) in
            (&mut o0, &o1, &o2, &o3, &o4, &o5, &o6, &o7, &o8).join() {
            o0.0 += o1.0 + o2.0 + o3.0 + o4.0 + o5.0 + o6.0 + o7.0 + o8.0;
        }
    }
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Sys_1024;
impl<'a> System<'a> for Sys_1024 {

    type SystemData = (WriteStorage<'a, Comp_i128_0>, ReadStorage<'a, Comp_i128_1>,
                       ReadStorage<'a, Comp_i128_2>, ReadStorage<'a, Comp_i128_3>,
                       ReadStorage<'a, Comp_i128_4>, ReadStorage<'a, Comp_i128_5>,
                       ReadStorage<'a, Comp_i128_6>, ReadStorage<'a, Comp_i128_7>);

    fn run(&mut self, (mut o0, o1, o2, o3, o4, o5, o6, o7): Self::SystemData) {
        // The `.join()` combines multiple component storages,
        // so we get access to all entities which have
        // both a position and a velocity.
        for (o0, o1, o2, o3, o4, o5, o6, o7) in
            (&mut o0, &o1, &o2, &o3, &o4, &o5, &o6, &o7).join() {
            o0.0 += o1.0 + o2.0 + o3.0 + o4.0 + o5.0 + o6.0 + o7.0;
        }
    }
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Sys_896;
impl<'a> System<'a> for Sys_896 {

    type SystemData = (WriteStorage<'a, Comp_i128_0>, ReadStorage<'a, Comp_i128_1>,
                       ReadStorage<'a, Comp_i128_2>, ReadStorage<'a, Comp_i128_3>,
                       ReadStorage<'a, Comp_i128_4>, ReadStorage<'a, Comp_i128_5>,
                       ReadStorage<'a, Comp_i128_6>);

    fn run(&mut self, (mut o0, o1, o2, o3, o4, o5, o6): Self::SystemData) {
        // The `.join()` combines multiple component storages,
        // so we get access to all entities which have
        // both a position and a velocity.
        for (o0, o1, o2, o3, o4, o5, o6) in
            (&mut o0, &o1, &o2, &o3, &o4, &o5, &o6).join() {
            o0.0 += o1.0 + o2.0 + o3.0 + o4.0 + o5.0 + o6.0;
        }
    }
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Sys_768;
impl<'a> System<'a> for Sys_768 {

    type SystemData = (WriteStorage<'a, Comp_i128_0>, ReadStorage<'a, Comp_i128_1>,
                       ReadStorage<'a, Comp_i128_2>, ReadStorage<'a, Comp_i128_3>,
                       ReadStorage<'a, Comp_i128_4>, ReadStorage<'a, Comp_i128_5>);

    fn run(&mut self, (mut o0, o1, o2, o3, o4, o5): Self::SystemData) {
        // The `.join()` combines multiple component storages,
        // so we get access to all entities which have
        // both a position and a velocity.
        for (o0, o1, o2, o3, o4, o5) in
            (&mut o0, &o1, &o2, &o3, &o4, &o5).join() {
            o0.0 += o1.0 + o2.0 + o3.0 + o4.0 + o5.0;
        }
    }
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Sys_640;
impl<'a> System<'a> for Sys_640 {

    type SystemData = (WriteStorage<'a, Comp_i128_0>, ReadStorage<'a, Comp_i128_1>,
                       ReadStorage<'a, Comp_i128_2>, ReadStorage<'a, Comp_i128_3>,
                       ReadStorage<'a, Comp_i128_4>);

    fn run(&mut self, (mut o0, o1, o2, o3, o4): Self::SystemData) {
        // The `.join()` combines multiple component storages,
        // so we get access to all entities which have
        // both a position and a velocity.
        for (o0, o1, o2, o3, o4) in
            (&mut o0, &o1, &o2, &o3, &o4).join() {
            o0.0 += o1.0 + o2.0 + o3.0 + o4.0;
        }
    }
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Sys_512;
impl<'a> System<'a> for Sys_512 {

    type SystemData = (WriteStorage<'a, Comp_i128_0>, ReadStorage<'a, Comp_i128_1>,
                       ReadStorage<'a, Comp_i128_2>, ReadStorage<'a, Comp_i128_3>);

    fn run(&mut self, (mut o0, o1, o2, o3): Self::SystemData) {
        // The `.join()` combines multiple component storages,
        // so we get access to all entities which have
        // both a position and a velocity.
        for (o0, o1, o2, o3) in
            (&mut o0, &o1, &o2, &o3).join() {
            o0.0 += o1.0 + o2.0 + o3.0;
        }
    }
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Sys_384;
impl<'a> System<'a> for Sys_384 {

    type SystemData = (WriteStorage<'a, Comp_i128_0>, ReadStorage<'a, Comp_i128_1>,
                       ReadStorage<'a, Comp_i128_2>);

    fn run(&mut self, (mut o0, o1, o2): Self::SystemData) {
        // The `.join()` combines multiple component storages,
        // so we get access to all entities which have
        // both a position and a velocity.
        for (o0, o1, o2) in
            (&mut o0, &o1, &o2).join() {
            o0.0 += o1.0 + o2.0;
        }
    }
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Sys_256;
impl<'a> System<'a> for Sys_256 {

    type SystemData = (WriteStorage<'a, Comp_i128_0>, ReadStorage<'a, Comp_i128_1>);

    fn run(&mut self, (mut o0, o1): Self::SystemData) {
        // The `.join()` combines multiple component storages,
        // so we get access to all entities which have
        // both a position and a velocity.
        for (o0, o1) in
            (&mut o0, &o1).join() {
            o0.0 += o1.0;
        }
    }
}