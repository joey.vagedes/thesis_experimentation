pub trait Exp6: Send {
    fn new(val: i128)->Self;
    fn run(&mut self);
}

pub struct Exp2048(pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128
);

impl Exp6 for Exp2048 {
    fn new(val: i128)->Self {
        return Exp2048(val,val,val,val,val,val,val,val,val,val,val,val,val,val,val,val);
    }

    fn run(&mut self) {
        self.0 += self.1 + self.2 + self.3 + self.4 + self.5 + self.6 + self.7 + self.8 + self.9 + self.10 + self.11 + self.12 + self.13 + self.14 + self.15;
    }
}

pub struct Exp1912(pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128
);

impl Exp6 for Exp1912 {
    fn new(val: i128)->Self {
        return Exp1912(val,val,val,val,val,val,val,val,val,val,val,val,val,val,val,val);
    }

    fn run(&mut self) {
        self.0 += self.2 + self.3 + self.4 + self.5 + self.6 + self.7 + self.8 + self.9 + self.10 + self.11 + self.12 + self.13 + self.14 + self.15;
    }
}

pub struct Exp1792(pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128
);

impl Exp6 for Exp1792 {
    fn new(val: i128)->Self {
        return Exp1792(val,val,val,val,val,val,val,val,val,val,val,val,val,val,val,val);
    }

    fn run(&mut self) {
        self.0 += self.3 + self.4 + self.5 + self.6 + self.7 + self.8 + self.9 + self.10 + self.11 + self.12 + self.13 + self.14 + self.15;
    }
}

pub struct Exp1664(pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128
);

impl Exp6 for Exp1664 {
    fn new(val: i128)->Self {
        return Exp1664(val,val,val,val,val,val,val,val,val,val,val,val,val,val,val,val);
    }

    fn run(&mut self) {
        self.0 += self.4 + self.5 + self.6 + self.7 + self.8 + self.9 + self.10 + self.11 + self.12 + self.13 + self.14 + self.15;
    }
}

pub struct Exp1536(pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128
);

impl Exp6 for Exp1536 {
    fn new(val: i128)->Self {
        return Exp1536(val,val,val,val,val,val,val,val,val,val,val,val,val,val,val,val);
    }

    fn run(&mut self) {
        self.0 += self.5 + self.6 + self.7 + self.8 + self.9 + self.10 + self.11 + self.12 + self.13 + self.14 + self.15;
    }
}

pub struct Exp1408(pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128
);

impl Exp6 for Exp1408 {
    fn new(val: i128)->Self {
        return Exp1408(val,val,val,val,val,val,val,val,val,val,val,val,val,val,val,val);
    }

    fn run(&mut self) {
        self.0 += self.6 + self.7 + self.8 + self.9 + self.10 + self.11 + self.12 + self.13 + self.14 + self.15;
    }
}

pub struct Exp1280(pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128
);

impl Exp6 for Exp1280 {
    fn new(val: i128)->Self {
        return Exp1280(val,val,val,val,val,val,val,val,val,val,val,val,val,val,val,val);
    }

    fn run(&mut self) {
        self.0 += self.7 + self.8 + self.9 + self.10 + self.11 + self.12 + self.13 + self.14 + self.15;
    }
}

pub struct Exp1152(pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128
);

impl Exp6 for Exp1152 {
    fn new(val: i128)->Self {
        return Exp1152(val,val,val,val,val,val,val,val,val,val,val,val,val,val,val,val);
    }

    fn run(&mut self) {
        self.0 += self.8 + self.9 + self.10 + self.11 + self.12 + self.13 + self.14 + self.15;
    }
}

pub struct Exp1024(pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128
);

impl Exp6 for Exp1024 {
    fn new(val: i128)->Self {
        return Exp1024(val,val,val,val,val,val,val,val,val,val,val,val,val,val,val,val);
    }

    fn run(&mut self) {
        self.0 += self.9 + self.10 + self.11 + self.12 + self.13 + self.14 + self.15;
    }
}

pub struct Exp896(pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128
);

impl Exp6 for Exp896 {
    fn new(val: i128)->Self {
        return Exp896(val,val,val,val,val,val,val,val,val,val,val,val,val,val,val,val);
    }

    fn run(&mut self) {
        self.0 += self.10 + self.11 + self.12 + self.13 + self.14 + self.15;
    }
}

pub struct Exp768(pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128
);

impl Exp6 for Exp768 {
    fn new(val: i128)->Self {
        return Exp768(val,val,val,val,val,val,val,val,val,val,val,val,val,val,val,val);
    }

    fn run(&mut self) {
        self.0 += self.11 + self.12 + self.13 + self.14 + self.15;
    }
}

pub struct Exp640(pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128
);

impl Exp6 for Exp640 {
    fn new(val: i128)->Self {
        return Exp640(val,val,val,val,val,val,val,val,val,val,val,val,val,val,val,val);
    }

    fn run(&mut self) {
        self.0 += self.12 + self.13 + self.14 + self.15;
    }
}

pub struct Exp512(pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128
);

impl Exp6 for Exp512 {
    fn new(val: i128)->Self {
        return Exp512(val,val,val,val,val,val,val,val,val,val,val,val,val,val,val,val);
    }

    fn run(&mut self) {
        self.0 += self.13 + self.14 + self.15;
    }
}

pub struct Exp384(pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128
);

impl Exp6 for Exp384 {
    fn new(val: i128)->Self {
        return Exp384(val,val,val,val,val,val,val,val,val,val,val,val,val,val,val,val);
    }

    fn run(&mut self) {
        self.0 += self.14 + self.15;
    }
}

pub struct Exp256(pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128,
                   pub i128
);

impl Exp6 for Exp256 {
    fn new(val: i128)->Self {
        return Exp256(val,val,val,val,val,val,val,val,val,val,val,val,val,val,val,val);
    }

    fn run(&mut self) {
        self.0 += self.15;
    }
}

