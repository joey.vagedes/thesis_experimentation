fn fib(n: i128)->i128
{
    if n <= 1 {

        return n;
    }
    return fib(n-1) + fib(n-2);
}

pub trait Exp5: Send {
    fn run0(&mut self);
    fn run1(&mut self);
    fn run2(&mut self);
    fn run3(&mut self);
    fn run4(&mut self);
    fn run5(&mut self);
    fn run6(&mut self);
    fn run7(&mut self);
    fn run8(&mut self);
    fn run9(&mut self);
    fn run10(&mut self);
    fn run11(&mut self);
    fn run12(&mut self);
    fn run13(&mut self);
    fn run14(&mut self);
    fn new(val: i128)->Self;
}
pub struct Obj2048(pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128,
                   pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128);

impl Exp5 for Obj2048 {
    fn run0(&mut self) {self.15 += fib(self.0); }
    fn run1(&mut self) {self.14 += fib(self.0); }
    fn run2(&mut self) {self.13 += fib(self.0); }
    fn run3(&mut self) {self.12 += fib(self.0); }
    fn run4(&mut self) {self.11 += fib(self.0); }
    fn run5(&mut self) {self.10 += fib(self.0); }
    fn run6(&mut self) {self.9 += fib(self.0); }
    fn run7(&mut self) {self.8 += fib(self.0); }
    fn run8(&mut self) {self.7 += fib(self.0); }
    fn run9(&mut self) {self.6 += fib(self.0); }
    fn run10(&mut self) {self.5 += fib(self.0); }
    fn run11(&mut self) {self.4 += fib(self.0); }
    fn run12(&mut self) {self.3 += fib(self.0); }
    fn run13(&mut self) {self.2 += fib(self.0); }
    fn run14(&mut self) {self.1 += fib(self.0); }
    fn new(val: i128)->Self {
        return Obj2048(val,val,val,val,val,val,val,val,val,val,val,val,val,val,val,val);
    }
}