use super::oop_obj::*;
use std::sync::{Arc, RwLock};
use rayon::ThreadPoolBuilder;
use rayon::iter::IntoParallelRefMutIterator;

type ThreadPoolWrapper = Option<::std::sync::Arc<::rayon::ThreadPool>>;

pub fn obj_setup<T: Exp5>(entity_count: i32, thread_count: usize) -> Vec<Vec<T>> {

    let mut vec: Vec<Vec<T>> = Vec::new();
    for i in 0..thread_count {
        vec.push(Vec::new());
        for _ in 0..entity_count/(thread_count as i32) {
            vec.get_mut(i).unwrap().push(T::new(criterion::black_box(5)));
        }
    }
    return vec;
}

//----------------------------------------------------------
pub struct OOPWorld<T: Exp5> {
    stages: Vec<Stage<T>>,
    pool: Arc<RwLock<ThreadPoolWrapper>>,
}

impl <T: Exp5> OOPWorld <T> {
    pub fn new(vec: Vec<Vec<T>>, thread_count: usize)->OOPWorld<T>{
        let pool: ThreadPoolWrapper = Some(Arc::from(ThreadPoolBuilder::new().num_threads(thread_count).build().unwrap()));
        let pool: Arc<RwLock<ThreadPoolWrapper>> = Arc::from(RwLock::from(pool));

        let stage: Stage<T> = Stage::new(vec);
        let mut stages: Vec<Stage<T>> = Vec::new();
        stages.push(stage);

        return OOPWorld{
            stages,
            pool,
        };
    }

    pub fn execute(&mut self){
        let stages = &mut self.stages;

        self.pool
            .read()
            .unwrap()
            .as_ref()
            .unwrap()
            .install(move || {
                for stage in stages {
                    stage.execute();
                }
            });
    }
}

//----------------------------------------------------------

struct Stage<T: Exp5> {
    groups: Vec<Vec<T>>
}

impl <T: Exp5> Stage <T> {
    fn new(vec: Vec<Vec<T>>)-> Stage<T> {

        let groups = vec;

        return Stage {
            groups
        };
    }

    fn execute(&mut self) {
        use rayon::iter::ParallelIterator;
        self.groups.par_iter_mut().for_each(|group| {
            for obj in group {
                obj.run0();
                obj.run1();
                obj.run2();
                obj.run3();
                obj.run4();
                obj.run5();
                obj.run6();
                obj.run7();
                obj.run8();
                obj.run9();
                obj.run10();
                obj.run11();
                obj.run12();
                obj.run13();
                obj.run14();
            }
        })
    }
}

