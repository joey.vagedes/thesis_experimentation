use specs::prelude::*;
use std::io;
use super::dod_system::*;
use super::dod_component::*;
use std::sync::Arc;

pub fn setup_component(world: &mut World) -> io::Result<()> {
    world.register::<Comp_i128_0>();
    world.register::<Comp_i128_1>();
    world.register::<Comp_i128_2>();
    world.register::<Comp_i128_3>();
    world.register::<Comp_i128_4>();
    world.register::<Comp_i128_5>();
    world.register::<Comp_i128_6>();
    world.register::<Comp_i128_7>();
    world.register::<Comp_i128_8>();
    world.register::<Comp_i128_9>();
    world.register::<Comp_i128_10>();
    world.register::<Comp_i128_11>();
    world.register::<Comp_i128_12>();
    world.register::<Comp_i128_13>();
    world.register::<Comp_i128_14>();
    world.register::<Comp_i128_15>();
    return Ok(());
}

pub fn setup_entity(world: &mut World) -> io::Result<()> {
    for _ in 0..1000 {
        world.create_entity()
            .with(Comp_i128_0(criterion::black_box(5)))
            .with(Comp_i128_1(criterion::black_box(5)))
            .with(Comp_i128_2(criterion::black_box(5)))
            .with(Comp_i128_3(criterion::black_box(5)))
            .with(Comp_i128_4(criterion::black_box(5)))
            .with(Comp_i128_5(criterion::black_box(5)))
            .with(Comp_i128_6(criterion::black_box(5)))
            .with(Comp_i128_7(criterion::black_box(5)))
            .with(Comp_i128_8(criterion::black_box(5)))
            .with(Comp_i128_9(criterion::black_box(5)))
            .with(Comp_i128_10(criterion::black_box(5)))
            .with(Comp_i128_11(criterion::black_box(5)))
            .with(Comp_i128_12(criterion::black_box(5)))
            .with(Comp_i128_13(criterion::black_box(5)))
            .with(Comp_i128_14(criterion::black_box(5)))
            .with(Comp_i128_15(criterion::black_box(5)))
            .build();
    }
    return Ok(())
}

pub fn setup_dispatcher<'a, 'b>(thread_count: usize)->Dispatcher<'a, 'b> {
    let pool = Arc::from(rayon::ThreadPoolBuilder::new().num_threads(thread_count).build().unwrap());

    let dispatcher = DispatcherBuilder::new()
        .with_pool(pool)
        .with(Sys_256bit_0, "sys0", &[])
        .with(Sys_256bit_1, "sys1", &[])
        .with(Sys_256bit_2, "sys2", &[])
        .with(Sys_256bit_3, "sys3", &[])
        .with(Sys_256bit_4, "sys4", &[])
        .with(Sys_256bit_5, "sys5", &[])
        .with(Sys_256bit_6, "sys6", &[])
        .with(Sys_256bit_7, "sys7", &[])
        .with(Sys_256bit_8, "sys8", &[])
        .with(Sys_256bit_9, "sys9", &[])
        .with(Sys_256bit_10, "sys10", &[])
        .with(Sys_256bit_11, "sys11", &[])
        .with(Sys_256bit_12, "sys12", &[])
        .with(Sys_256bit_13, "sys13", &[])
        .with(Sys_256bit_14, "sys14", &[])
        .build();
    return dispatcher;
}