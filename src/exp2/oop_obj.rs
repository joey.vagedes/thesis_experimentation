pub trait Exp2: Send {
    fn run(&mut self);
    fn new(val: i128)->Self;
}

pub struct Obj1024(pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128, pub i128);
impl Exp2 for Obj1024 {
    fn run(&mut self) {
        self.0 += self.7;
    }
    fn new(val: i128)->Self{
        return Obj1024(val,val,val,val,val,val,val,val);
    }
}