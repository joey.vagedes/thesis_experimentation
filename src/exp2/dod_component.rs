use specs::prelude::*;

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_0(pub i128);
impl Component for Comp_i128_0 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_1(pub i128);
impl Component for Comp_i128_1 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_2(pub i128);
impl Component for Comp_i128_2 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_3(pub i128);
impl Component for Comp_i128_3 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_4(pub i128);
impl Component for Comp_i128_4 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_5(pub i128);
impl Component for Comp_i128_5 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_6(pub i128);
impl Component for Comp_i128_6 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_7(pub i128);
impl Component for Comp_i128_7 {
    type Storage = DenseVecStorage<Self>;
}