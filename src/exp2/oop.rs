use std::sync::{Arc, RwLock};
use rayon::*;
use rayon::iter::IntoParallelRefMutIterator;
use crate::exp2::oop_obj::*;

type ThreadPoolWrapper = Option<::std::sync::Arc<::rayon::ThreadPool>>;

pub fn obj_setup<T: Exp2>(entity_count: i32)-> Vec<T> {

    let mut vec: Vec<T> = Vec::new();
    for _ in 0..entity_count {
        let tmp = T::new(criterion::black_box(5));
        vec.push(tmp);
    }

    return vec;
}

//----------------------------------------------------------
pub struct OOPWorld<T: Exp2> {
    stages: Vec<Stage<T>>,
    pool: Arc<RwLock<ThreadPoolWrapper>>
}

impl <T: Exp2> OOPWorld <T> {
    pub fn new(vec: Vec<T>, thread_count: usize)->OOPWorld<T>{
        let pool: ThreadPoolWrapper = Some(Arc::from(ThreadPoolBuilder::new().num_threads(thread_count).build().unwrap()));
        let pool: Arc<RwLock<ThreadPoolWrapper>> = Arc::from(RwLock::from(pool));

        let stage: Stage<T> = Stage::new(vec);
        let mut stages: Vec<Stage<T>> = Vec::new();
        stages.push(stage);

        return OOPWorld{
            stages,
            pool
        };
    }

    pub fn execute(&mut self){
        let stages = &mut self.stages;
        self.pool
            .read()
            .unwrap()
            .as_ref()
            .unwrap()
            .install(move || {
                for stage in stages {
                    stage.execute();
                }
            });
    }
}

//----------------------------------------------------------

struct Stage<T: Exp2> {
    groups: Vec<Vec<T>>
}

impl <T: Exp2> Stage <T> {
    fn new(vec: Vec<T>)-> Stage<T> {

        let mut groups: Vec<Vec<T>> = Vec::new();
        groups.push(vec);

        return Stage {
            groups
        };
    }

    fn execute(&mut self) {
        use rayon::iter::ParallelIterator;
        self.groups.par_iter_mut().for_each(|group| {
            for obj in group {
                obj.run();
            }
        })
    }
}

