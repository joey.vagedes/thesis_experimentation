use specs::{World, WorldExt, Builder, Dispatcher, DispatcherBuilder};
use std::io;
use super::dod_component::*;
use super::dod_system::*;
use std::sync::Arc;

pub fn setup_component(world: &mut World) -> io::Result<()> {
    world.register::<Comp_i128_0>();
    world.register::<Comp_i128_1>();
    world.register::<Comp_i128_2>();
    world.register::<Comp_i128_3>();
    world.register::<Comp_i128_4>();
    world.register::<Comp_i128_5>();
    world.register::<Comp_i128_6>();
    world.register::<Comp_i128_7>();
    return Ok(());
}

pub fn setup_entity(entity_count: i32, world: &mut World) -> io::Result<()> {
    for _ in 0..entity_count {
        world.create_entity()
            .with(Comp_i128_0(criterion::black_box(5)))
            .with(Comp_i128_1(criterion::black_box(5)))
            .with(Comp_i128_2(criterion::black_box(5)))
            .with(Comp_i128_3(criterion::black_box(5)))
            .with(Comp_i128_4(criterion::black_box(5)))
            .with(Comp_i128_5(criterion::black_box(5)))
            .with(Comp_i128_6(criterion::black_box(5)))
            .with(Comp_i128_7(criterion::black_box(5)))
            .build();
    }

    return Ok(());
}

pub fn setup_dispatcher<'a, 'b>()->Dispatcher<'a, 'b> {

    let pool = Arc::from(rayon::ThreadPoolBuilder::new().num_threads(1).build().unwrap());
    let dispatcher = DispatcherBuilder::new()
        .with_pool(pool)
        .with(Sys_256bit_0, "sys", &[])
        .build();

    return dispatcher
}