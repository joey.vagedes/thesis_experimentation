use rayon::iter::IntoParallelRefMutIterator;
use rayon::*;
use std::sync::{Arc, RwLock};
use super::oop_obj::*;
pub type ThreadPoolWrapper = Option<::std::sync::Arc<::rayon::ThreadPool>>;

//Responsible for spawning the objects for the experiment
pub fn obj_setup<T: Exp1>()-> Vec<T> {

    let mut vec: Vec<T> = Vec::new();
    for _ in 0..5000 {
        let tmp = T::new(criterion::black_box(5));
        vec.push(tmp);
    }

    return vec;
}

//Struct to imitate the World in ECS Architecture
//Stages are set up to match that of the ECS World
pub struct OOPWorld<T: Exp1> {
    stages: Vec<Stage<T>>,
    pool: Arc<RwLock<ThreadPoolWrapper>>
}
impl <T: Exp1> OOPWorld <T> {
    pub fn new(vec: Vec<T>, thread_count: usize)->OOPWorld<T>{
        let pool: ThreadPoolWrapper = Some(Arc::from(
            ThreadPoolBuilder::new().num_threads(thread_count).build().unwrap()));
        let pool: Arc<RwLock<ThreadPoolWrapper>> = Arc::from(RwLock::from(pool));

        let stage: Stage<T> = Stage::new(vec);
        let mut stages: Vec<Stage<T>> = Vec::new();
        stages.push(stage);

        return OOPWorld{
            stages,
            pool
        };
    }

    //Executes all methods in the same manner as the ECS Architecture
    pub fn execute(&mut self){
        let stages = &mut self.stages;
        self.pool
            .read()
            .unwrap()
            .as_ref()
            .unwrap()
            .install(move || {
                for stage in stages {
                    stage.execute();
                }
            });
    }
}

//Struct to imitate the Stage in ECS Architecture
struct Stage<T: Exp1> {
    groups: Vec<Vec<T>>
}
impl <T: Exp1> Stage <T> {
    fn new(vec: Vec<T>)-> Stage<T> {

        let mut groups: Vec<Vec<T>> = Vec::new();
        groups.push(vec);

        return Stage {
            groups
        };
    }
    fn execute(&mut self) {
        use rayon::iter::ParallelIterator;
        self.groups.par_iter_mut().for_each(|group| {
            for obj in group {
                obj.run();
            }
        })
    }
}