use specs::prelude::*;

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i64_0(pub i64);
impl Component for Comp_i64_0 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i64_1(pub i64);
impl Component for Comp_i64_1 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_0(pub i128);
impl Component for Comp_i128_0 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_1(pub i128);
impl Component for Comp_i128_1 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_2(pub i128);
impl Component for Comp_i128_2 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_3(pub i128);
impl Component for Comp_i128_3 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_4(pub i128);
impl Component for Comp_i128_4 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_5(pub i128);
impl Component for Comp_i128_5 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_6(pub i128);
impl Component for Comp_i128_6 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_7(pub i128);
impl Component for Comp_i128_7 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_8(pub i128);
impl Component for Comp_i128_8 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_9(pub i128);
impl Component for Comp_i128_9 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_10(pub i128);
impl Component for Comp_i128_10 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_11(pub i128);
impl Component for Comp_i128_11 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_12(pub i128);
impl Component for Comp_i128_12 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_13(pub i128);
impl Component for Comp_i128_13 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_14(pub i128);
impl Component for Comp_i128_14 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_15(pub i128);
impl Component for Comp_i128_15 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_16(pub i128);
impl Component for Comp_i128_16 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_17(pub i128);
impl Component for Comp_i128_17 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_18(pub i128);
impl Component for Comp_i128_18 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_19(pub i128);
impl Component for Comp_i128_19 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_20(pub i128);
impl Component for Comp_i128_20 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_21(pub i128);
impl Component for Comp_i128_21 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_22(pub i128);
impl Component for Comp_i128_22 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_23(pub i128);
impl Component for Comp_i128_23 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_24(pub i128);
impl Component for Comp_i128_24 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_25(pub i128);
impl Component for Comp_i128_25 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_26(pub i128);
impl Component for Comp_i128_26 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_27(pub i128);
impl Component for Comp_i128_27 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_28(pub i128);
impl Component for Comp_i128_28 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_29(pub i128);
impl Component for Comp_i128_29 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_30(pub i128);
impl Component for Comp_i128_30 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_31(pub i128);
impl Component for Comp_i128_31 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_32(pub i128);
impl Component for Comp_i128_32 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_33(pub i128);
impl Component for Comp_i128_33 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_34(pub i128);
impl Component for Comp_i128_34 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_35(pub i128);
impl Component for Comp_i128_35 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_36(pub i128);
impl Component for Comp_i128_36 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_37(pub i128);
impl Component for Comp_i128_37 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_38(pub i128);
impl Component for Comp_i128_38 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_39(pub i128);
impl Component for Comp_i128_39 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_40(pub i128);
impl Component for Comp_i128_40 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_41(pub i128);
impl Component for Comp_i128_41 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_42(pub i128);
impl Component for Comp_i128_42 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_43(pub i128);
impl Component for Comp_i128_43 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_44(pub i128);
impl Component for Comp_i128_44 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_45(pub i128);
impl Component for Comp_i128_45 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_46(pub i128);
impl Component for Comp_i128_46 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_47(pub i128);
impl Component for Comp_i128_47 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_48(pub i128);
impl Component for Comp_i128_48 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_49(pub i128);
impl Component for Comp_i128_49 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_50(pub i128);
impl Component for Comp_i128_50 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_51(pub i128);
impl Component for Comp_i128_51 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_52(pub i128);
impl Component for Comp_i128_52 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_53(pub i128);
impl Component for Comp_i128_53 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_54(pub i128);
impl Component for Comp_i128_54 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_55(pub i128);
impl Component for Comp_i128_55 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_56(pub i128);
impl Component for Comp_i128_56 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_57(pub i128);
impl Component for Comp_i128_57 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_58(pub i128);
impl Component for Comp_i128_58 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_59(pub i128);
impl Component for Comp_i128_59 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_60(pub i128);
impl Component for Comp_i128_60 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_61(pub i128);
impl Component for Comp_i128_61 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_62(pub i128);
impl Component for Comp_i128_62 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_63(pub i128);
impl Component for Comp_i128_63 {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_64(pub i128);
impl Component for Comp_i128_64{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_65(pub i128);
impl Component for Comp_i128_65{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_66(pub i128);
impl Component for Comp_i128_66{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_67(pub i128);
impl Component for Comp_i128_67{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_68(pub i128);
impl Component for Comp_i128_68{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_69(pub i128);
impl Component for Comp_i128_69{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_70(pub i128);
impl Component for Comp_i128_70{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_71(pub i128);
impl Component for Comp_i128_71{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_72(pub i128);
impl Component for Comp_i128_72{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_73(pub i128);
impl Component for Comp_i128_73{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_74(pub i128);
impl Component for Comp_i128_74{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_75(pub i128);
impl Component for Comp_i128_75{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_76(pub i128);
impl Component for Comp_i128_76{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_77(pub i128);
impl Component for Comp_i128_77{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_78(pub i128);
impl Component for Comp_i128_78{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_79(pub i128);
impl Component for Comp_i128_79{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_80(pub i128);
impl Component for Comp_i128_80{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_81(pub i128);
impl Component for Comp_i128_81{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_82(pub i128);
impl Component for Comp_i128_82{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_83(pub i128);
impl Component for Comp_i128_83{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_84(pub i128);
impl Component for Comp_i128_84{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_85(pub i128);
impl Component for Comp_i128_85{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_86(pub i128);
impl Component for Comp_i128_86{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_87(pub i128);
impl Component for Comp_i128_87{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_88(pub i128);
impl Component for Comp_i128_88{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_89(pub i128);
impl Component for Comp_i128_89{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_90(pub i128);
impl Component for Comp_i128_90{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_91(pub i128);
impl Component for Comp_i128_91{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_92(pub i128);
impl Component for Comp_i128_92{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_93(pub i128);
impl Component for Comp_i128_93{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_94(pub i128);
impl Component for Comp_i128_94{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_95(pub i128);
impl Component for Comp_i128_95{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_96(pub i128);
impl Component for Comp_i128_96{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_97(pub i128);
impl Component for Comp_i128_97{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_98(pub i128);
impl Component for Comp_i128_98{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_99(pub i128);
impl Component for Comp_i128_99{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_100(pub i128);
impl Component for Comp_i128_100{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_101(pub i128);
impl Component for Comp_i128_101{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_102(pub i128);
impl Component for Comp_i128_102{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_103(pub i128);
impl Component for Comp_i128_103{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_104(pub i128);
impl Component for Comp_i128_104{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_105(pub i128);
impl Component for Comp_i128_105{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_106(pub i128);
impl Component for Comp_i128_106{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_107(pub i128);
impl Component for Comp_i128_107{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_108(pub i128);
impl Component for Comp_i128_108{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_109(pub i128);
impl Component for Comp_i128_109{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_110(pub i128);
impl Component for Comp_i128_110{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_111(pub i128);
impl Component for Comp_i128_111{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_112(pub i128);
impl Component for Comp_i128_112{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_113(pub i128);
impl Component for Comp_i128_113{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_114(pub i128);
impl Component for Comp_i128_114{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_115(pub i128);
impl Component for Comp_i128_115{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_116(pub i128);
impl Component for Comp_i128_116{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_117(pub i128);
impl Component for Comp_i128_117{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_118(pub i128);
impl Component for Comp_i128_118{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_119(pub i128);
impl Component for Comp_i128_119{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_120(pub i128);
impl Component for Comp_i128_120{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_121(pub i128);
impl Component for Comp_i128_121{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_122(pub i128);
impl Component for Comp_i128_122{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_123(pub i128);
impl Component for Comp_i128_123{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_124(pub i128);
impl Component for Comp_i128_124{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_125(pub i128);
impl Component for Comp_i128_125{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_126(pub i128);
impl Component for Comp_i128_126{
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub struct Comp_i128_127(pub i128);
impl Component for Comp_i128_127{
    type Storage = DenseVecStorage<Self>;
}